

/**
 * Created by joshuataylor on 10/9/15.
 */
public class S4_Barcodes_Medium {

    //UPCA Encoder Instance Variables
    private String userUPCodeToEncode = "";

    //UPCA Decoder Instance Variables
    private String userUPCodeToDecode = "";

    //PostNetEncoder Instance Variables
    private String userPostNetCodeToEncode;
    private String binaryRepresentationPostNetEncode = "";
    private String longShortRepresentation = "";


    //PostNetDecode Instance Variables
    private  String userPostNetCodeToDecode = "";
    private String decodedPostnetCode = "";
    private String verifiedCheckSumForDecodedPostnetCode = "";

    //UPCA
    //Encoder

    /**
     * setter method that sets setUserUPCodeToEncode
     * @param userUPCCode upc code to encode
     */
    public void setUserUPCodeToEncode(String userUPCCode) {
        if (userUPCCode.length() != 11) {
            throw new IllegalArgumentException("Number needs to be 11 digits long");
        }

        this.userUPCodeToEncode = userUPCCode;
    }

    /**
     *encodeUPCACode encodes upca value
     * @return string value of encoded upca code
     */
    public String encodeUPCACode() {

        String upcCode = "";
        for (int i = 0; i <= 14; i++) {
            if (i == 0 || i == 14) {
                upcCode = upcCode + UPCDigitCodes.getUPCStringValueLeft("10");
            } else if (i == 7) {
                upcCode = upcCode + UPCDigitCodes.getUPCStringValueLeft("11");
            } else if (i == 13) {
                upcCode = upcCode + this.calculateCheckBitUPCEncoder();
            } else {
                if (i < 7) {
                    char singleChar = this.userUPCodeToEncode.charAt(i - 1);
                    String charToString = String.valueOf(singleChar);
                    upcCode = upcCode + UPCDigitCodes.getUPCStringValueLeft(charToString);
                } else if ( i >= 7) {
                    char singleChar = this.userUPCodeToEncode.charAt(i-2);
                    String charToString = String.valueOf(singleChar);
                    upcCode = upcCode + UPCDigitCodes.getUPCStringValueRight(charToString);
                }
            }
        }
        return upcCode;
    }

    /**
     *calculateCheckBitUPCEncoder calculates teh check bit value used for the
     * converted upc value
     * @return string vaule of checkbit for the encoded upc value
     */
    private String calculateCheckBitUPCEncoder() {

        int checkBitInteger = 0;
        int totalUPCNumbers = 0;
        int evenUPCNumbers = 0;
        int oddUPCNumbers = 0;

        for (int i = 0; i < this.userUPCodeToEncode.length(); i++) {
            char singleChar = this.userUPCodeToEncode.charAt(i);
            totalUPCNumbers = totalUPCNumbers + Character.getNumericValue(singleChar);

        }

        for (int i = 0; i < this.userUPCodeToEncode.length()-1; i++) {
            if (i == 0) {
                char singleCha = this.userUPCodeToEncode.charAt(i);
                oddUPCNumbers = oddUPCNumbers + Character.getNumericValue(singleCha);
            }
            else {
                char singleCha = this.userUPCodeToEncode.charAt(i + 1);
                oddUPCNumbers = oddUPCNumbers + Character.getNumericValue(singleCha);
            }
        }

        evenUPCNumbers = totalUPCNumbers - oddUPCNumbers;

        checkBitInteger = (oddUPCNumbers * 3);
        checkBitInteger = (checkBitInteger + evenUPCNumbers);
        checkBitInteger = (checkBitInteger % 10);
        checkBitInteger = (10 - checkBitInteger);

        String checkBitStringValue = String.valueOf(checkBitInteger);

        return UPCDigitCodes.getUPCStringValueRight(checkBitStringValue);
    }

    //UPC Decoder

    /**
     *setter method used to set the instance variable userUPCodeToDecode
     * @param userUPCodeToDecode string value to set as userUPCodeToDecode
     */
    public void  setUserUPCodeToDecode (String userUPCodeToDecode) {
        if (userUPCodeToDecode.length() != 95) {
            throw new IllegalArgumentException("Number needs to be 88 digits long");
        }

        this.userUPCodeToDecode = userUPCodeToDecode;

    }

    /**
     * getter method that returns the instance variable userUPCodeToDecode
     * @return string of the instance variable userUPCodeToDecode
     */
    public String getUserUPCodeToDecode() {
        return userUPCodeToDecode;
    }

    /**
     *decodeUPCACode decodes a upca code
     * @return string value of upc code
     */
    public String decodeUPCACode() {

        String upcCodeInteger = "";
        String temporary = "";
        for (int i = 3; i < 45; i++) {
            char singleChar = this.userUPCodeToDecode.charAt(i);
            temporary = temporary + singleChar;
            if (temporary.length() == 7){
                upcCodeInteger = upcCodeInteger + UPCDigitCodes.getUPCIntegerFromBianaryLeft(temporary);
                temporary = "";
            }
        }
        for (int i = 50; i < 88; i++) {
            char singleChar = this.userUPCodeToDecode.charAt(i);
            temporary = temporary + singleChar;
            if (temporary.length() == 7){
                upcCodeInteger = upcCodeInteger + UPCDigitCodes.getUPCIntegerFromBianaryRight(temporary);
                temporary = "";
            }
        }
        return upcCodeInteger;
    }

    /**
     *getUPCACheckBitToDecode calulates the check of the UPCA code that is being decoded
     * @return string value of UPCA check to decode
     */
    public String getUPCACheckBitToDecode() {
        String temporary= "";
        String checkBit = "";
        for (int i = 85; i < 92; i++) {
            char singleChar = this.userUPCodeToDecode.charAt(i);
            temporary = temporary + singleChar;
            if (temporary.length() == 7){
                checkBit = UPCDigitCodes.getUPCIntegerFromBianaryRight(temporary);
            }
        }
        return checkBit;
    }

    //Postnet

    //Postnet Encoder

    /**
     *setter method used to set instance variable setUserPostNetCodeToEncode
     * @param userPostNetCodeToEncode string value of postnet code to encode
     */
    public void setUserPostNetCodeToEncode (String userPostNetCodeToEncode) {
        this.userPostNetCodeToEncode = userPostNetCodeToEncode;
    }

    /**
     * encodePostnetCode decodes a postnet code in long/short format and prints it to the screen
     */
    public void encodePostnetCode() {

        if (this.userPostNetCodeToEncode.length() == 5) {

            for (int i = 0; i <= 7; i++) {
                if (i == 0 || i == 7) {
                    this.binaryRepresentationPostNetEncode = this.binaryRepresentationPostNetEncode + ConvertPostNetValue.getPostnetBinaryValueFromNumber("11");
                    this.longShortRepresentation = this.longShortRepresentation + ConvertPostNetValue.getPostNetLongShortValueFromNummber("11");
                } else if (i == 6) {
                    this.binaryRepresentationPostNetEncode = this.binaryRepresentationPostNetEncode + ConvertPostNetValue.getPostnetBinaryValueFromNumber(calculateCheckBitPostNetEncode());
                    this.longShortRepresentation = this.longShortRepresentation + ConvertPostNetValue.getPostNetLongShortValueFromNummber(calculateCheckBitPostNetEncode());
                } else {
                    char singleChar = this.userPostNetCodeToEncode.charAt(i - 1);
                    String charToString = String.valueOf(singleChar);
                    this.binaryRepresentationPostNetEncode = this.binaryRepresentationPostNetEncode + ConvertPostNetValue.getPostnetBinaryValueFromNumber(charToString);
                    this.longShortRepresentation = this.longShortRepresentation + ConvertPostNetValue.getPostNetLongShortValueFromNummber(charToString);
                }
            }
        }

        if (this.userPostNetCodeToEncode.length() == 9) {

            for (int i = 0; i <= 11; i++) {
                if (i == 0 || i == 11) {
                    this.binaryRepresentationPostNetEncode = this.binaryRepresentationPostNetEncode + ConvertPostNetValue.getPostnetBinaryValueFromNumber("11");
                    this.longShortRepresentation = this.longShortRepresentation + ConvertPostNetValue.getPostNetLongShortValueFromNummber("11");
                } else if (i == 10) {
                    this.binaryRepresentationPostNetEncode = this.binaryRepresentationPostNetEncode + ConvertPostNetValue.getPostnetBinaryValueFromNumber(calculateCheckBitPostNetEncode());
                    this.longShortRepresentation = this.longShortRepresentation + ConvertPostNetValue.getPostNetLongShortValueFromNummber(calculateCheckBitPostNetEncode());
                } else {
                    char singleChar = this.userPostNetCodeToEncode.charAt(i - 1);
                    String charToString = String.valueOf(singleChar);
                    this.binaryRepresentationPostNetEncode = this.binaryRepresentationPostNetEncode + ConvertPostNetValue.getPostnetBinaryValueFromNumber(charToString);
                    this.longShortRepresentation = this.longShortRepresentation + ConvertPostNetValue.getPostNetLongShortValueFromNummber(charToString);
                }
            }

        }

        if (this.userPostNetCodeToEncode.length() == 11) {

            for (int i = 0; i <= 13; i++) {
                if (i == 0 || i == 13) {
                    this.binaryRepresentationPostNetEncode = this.binaryRepresentationPostNetEncode + ConvertPostNetValue.getPostnetBinaryValueFromNumber("11");
                    this.longShortRepresentation = this.longShortRepresentation + ConvertPostNetValue.getPostNetLongShortValueFromNummber("11");
                } else if (i == 12) {
                    this.binaryRepresentationPostNetEncode = this.binaryRepresentationPostNetEncode + ConvertPostNetValue.getPostnetBinaryValueFromNumber(calculateCheckBitPostNetEncode());
                    this.longShortRepresentation = this.longShortRepresentation + ConvertPostNetValue.getPostNetLongShortValueFromNummber(calculateCheckBitPostNetEncode());
                } else {
                    char singleChar = this.userPostNetCodeToEncode.charAt(i - 1);
                    String charToString = String.valueOf(singleChar);
                    this.binaryRepresentationPostNetEncode = this.binaryRepresentationPostNetEncode + ConvertPostNetValue.getPostnetBinaryValueFromNumber(charToString);
                    this.longShortRepresentation = this.longShortRepresentation + ConvertPostNetValue.getPostNetLongShortValueFromNummber(charToString);
                }
            }

        }

        System.out.println(this.toStringPostnetEncoder());
    }

    /**
     * calculateCheckBitPostNetEncode calculates the check bit
     * for the pstnet encoded code
     * @return string value of the calculatedCheckBit for the Postnet encoded code
     */
    private String calculateCheckBitPostNetEncode() {

        int count = 0;

        for (int i = 0; i < userPostNetCodeToEncode.length(); i++) {
            char testChar = userPostNetCodeToEncode.charAt(i);
            String charToString = String.valueOf(testChar);
            count = count + Integer.parseInt(charToString);
        }

        int checkBitValue =  10 - (count % 10);

        return String.valueOf(checkBitValue);
    }

    /**
     *toStringPostNetDecode formats a string for the Postnet encoded string
     * @return a string of the final postnet encoded string
     */
    public String toStringPostnetEncoder() {

        String formattedString = longShortRepresentation + "\n\n" + binaryRepresentationPostNetEncode + "\n";
        return formattedString;
    }

    /**
     *getter method that returns private variable binaryRepresentationPostNetEncode
     * @return string variable binaryRepresentationPostNetEncode
     */
    public String getBinaryRepresentationPostNetEncode() {
        return binaryRepresentationPostNetEncode;
    }

    /**
     *getter method that returns private variable longShortRepresentation
     * @return string variable longShortRepresentation
     */
    public String getLongShortRepresentation() {
        return longShortRepresentation;
    }

    //Postnet Decoder

    /**
     *getter method that returns private variable decodedPostnetCode
     * @return string variable decodedPostnetCode
     */
    public String getDecodedPostnetCode() {
        return decodedPostnetCode;
    }

    /**
     *setUserPostNetCodeToDecode sets the private variable userPostNetCodeToDecode
     * @param userPostNetCode is a string and is used to set variable userPostNetCodeToDecode
     */
    public void setUserPostNetCodeToDecode(String userPostNetCode){
        this.userPostNetCodeToDecode = userPostNetCode;
    }


    /**
     * decodeBinaryPostnetCode decodes a postnet code in binary format and prints it to the screen
     */
    public void decodeBinaryPostnetCode() {

        if (this.userPostNetCodeToDecode.length() == 32) {
            int count = 0;
            for (int i = 0; i < 26; i = i + 5) {
                if ( i < 22) {
                    String subString = userPostNetCodeToDecode.substring(i+1, (i + 6));
                    String convertedString = ConvertPostNetValue.getPostNetNumberFromBinary(subString);
                    this.decodedPostnetCode = this.decodedPostnetCode + convertedString;
                    count = Integer.parseInt(convertedString) + count;
                }
                else {
                    String subString = userPostNetCodeToDecode.substring(26, 31);
                    String convertedCheckSumString = ConvertPostNetValue.getPostNetNumberFromBinary(subString);
                    int checkSum = Integer.parseInt(convertedCheckSumString);
                    int checkSumPlusCount = checkSum + count;
                    if ((checkSumPlusCount % 10) == 0) {
                        this.verifiedCheckSumForDecodedPostnetCode = "CheckSum = " + convertedCheckSumString + " and valid";
                    } else{
                        this.verifiedCheckSumForDecodedPostnetCode = "CheckSum is invalid";
                    }

                }
            }


        }
        if (this.userPostNetCodeToDecode.length() == 52){
            int count = 0;
            for (int i = 0; i < 46; i = i + 5) {
                if ( i < 42) {
                    String subString = userPostNetCodeToDecode.substring(i+1, (i + 6));
                    String convertedString = ConvertPostNetValue.getPostNetNumberFromBinary(subString);
                    this.decodedPostnetCode = this.decodedPostnetCode + convertedString;
                    count = Integer.parseInt(convertedString) + count;
                }
                else {
                    String subString = userPostNetCodeToDecode.substring(46, 51);
                    String convertedCheckSumString = ConvertPostNetValue.getPostNetNumberFromBinary(subString);
                    int checkSum = Integer.parseInt(convertedCheckSumString);
                    int checkSumPlusCount = checkSum + count;
                    if ((checkSumPlusCount % 10) == 0) {
                        this.verifiedCheckSumForDecodedPostnetCode = "CheckSum = " + convertedCheckSumString + " and valid";
                    } else{
                        this.verifiedCheckSumForDecodedPostnetCode = "CheckSum is invalid";
                    }

                }
            }
        }
        if(this.userPostNetCodeToDecode.length() == 62){
            int count = 0;
            for (int i = 0; i < 56; i = i + 5) {
                if ( i < 52) {
                    String subString = userPostNetCodeToDecode.substring(i + 1, (i + 6));
                    String convertedString = ConvertPostNetValue.getPostNetNumberFromBinary(subString);
                    decodedPostnetCode = decodedPostnetCode + convertedString;
                    count = Integer.parseInt(convertedString) + count;
                }
                else {
                    String subString = userPostNetCodeToDecode.substring(56, 61);
                    String convertedCheckSumString = ConvertPostNetValue.getPostNetNumberFromBinary(subString);
                    int checkSum = Integer.parseInt(convertedCheckSumString);
                    int checkSumPlusCount = checkSum + count;
                    if ((checkSumPlusCount % 10) == 0) {
                        this.verifiedCheckSumForDecodedPostnetCode = "CheckSum = " + convertedCheckSumString + " and valid";
                    } else{
                        this.verifiedCheckSumForDecodedPostnetCode = "CheckSum is invalid";
                    }

                }
            }
        }

        System.out.println(this.toStringPostNetDecode());

    }

    /**
     *toStringPostNetDecode formats a string for the Postnet Decoded string
     * @return a string of the final postnet Decode string
     */
    public String toStringPostNetDecode(){

        String formattedString  = "Zip code = " + decodedPostnetCode + "\n\n" + verifiedCheckSumForDecodedPostnetCode + "\n\n";
        return  formattedString;


    }

}
