

/**
 * UPCDigitCodes is an Enumeration Class used to encode/decode UPC codes
 */
public enum UPCDigitCodes {

    GUARDBIT        ("101","101"),
    GUARDBITMIDDLE  ("01010", "01010"),
    ZERO            ("0001101", "1110010"),
    ONE             ("0011001", "1100110"),
    TWO             ("0010011", "1101100"),
    THREE           ("0111101", "1000010"),
    FOUR            ("0100011", "1011100"),
    FIVE            ("0110001", "1001110"),
    SIX             ("0101111", "1010000"),
    SEVEN           ("0111011", "1000100"),
    EIGHT           ("0110111", "1001000"),
    NINE            ("0001011", "1110100");

    private final String upcNumberLeft;
    private final String upcNumberRight;

    UPCDigitCodes(String upcNumberLeft, String upcNumberRight) {
        this.upcNumberLeft = upcNumberLeft;
        this.upcNumberRight = upcNumberRight;

    }

    /**
     *getUpcNumberLeft gets the number from the left
     * the UPCDigitCodes enum class
     * @return the upcNumberLeft value
     */
    public String getUpcNumberLeft() {
        return upcNumberLeft;
    }

    /**
     * getUpcNumberRight gets the number from the right
     * the UPCDigitCodes enum class
     * @return string value of instance varible upcNumberRight
     */
    public String getUpcNumberRight() {
        return upcNumberRight;
    }

    /**
     *getUPCStringValueLeft converts a number to its corresponding
     * upc code
     * @param singleNumberValue number to convert to upc code
     * @return a string value of the number entered
     */
    public static String getUPCStringValueLeft(String singleNumberValue) {

        int testInt = Integer.parseInt(singleNumberValue);

        switch (testInt){
            case 0:
                return UPCDigitCodes.ZERO.getUpcNumberLeft();
            case 1:
                return UPCDigitCodes.ONE.getUpcNumberLeft();
            case 2:
                return UPCDigitCodes.TWO.getUpcNumberLeft();
            case 3:
                return UPCDigitCodes.THREE.getUpcNumberLeft();
            case 4:
                return UPCDigitCodes.FOUR.getUpcNumberLeft();
            case 5:
                return UPCDigitCodes.FIVE.getUpcNumberLeft();
            case 6:
                return UPCDigitCodes.SIX.getUpcNumberLeft();
            case 7:
                return UPCDigitCodes.SEVEN.getUpcNumberLeft();
            case 8:
                return UPCDigitCodes.EIGHT.getUpcNumberLeft();
            case 9:
                return UPCDigitCodes.NINE.getUpcNumberLeft();
            case 10:
                return UPCDigitCodes.GUARDBIT.getUpcNumberLeft();
            case 11:
                return UPCDigitCodes.GUARDBITMIDDLE.getUpcNumberLeft();
        }
        return "Error";
    }

    /**
     *getUPCStringValueLeft converts a number to its corresponding
     * upc code
     * @param singleNumberValue number to convert to upc code
     * @return a string value of the number entered
     */
    public static String getUPCStringValueRight(String singleNumberValue) {

        int testInt = Integer.parseInt(singleNumberValue);

        switch (testInt){
            case 0:
                return UPCDigitCodes.ZERO.getUpcNumberRight();
            case 1:
                return UPCDigitCodes.ONE.getUpcNumberRight();
            case 2:
                return UPCDigitCodes.TWO.getUpcNumberRight();
            case 3:
                return UPCDigitCodes.THREE.getUpcNumberRight();
            case 4:
                return UPCDigitCodes.FOUR.getUpcNumberRight();
            case 5:
                return UPCDigitCodes.FIVE.getUpcNumberRight();
            case 6:
                return UPCDigitCodes.SIX.getUpcNumberRight();
            case 7:
                return UPCDigitCodes.SEVEN.getUpcNumberRight();
            case 8:
                return UPCDigitCodes.EIGHT.getUpcNumberRight();
            case 9:
                return UPCDigitCodes.NINE.getUpcNumberRight();
            case 10:
                return UPCDigitCodes.GUARDBIT.getUpcNumberRight();
            case 11:
                return UPCDigitCodes.GUARDBITMIDDLE.getUpcNumberRight();
        }
        return "Error";
    }

    /**
     * getUPCIntegerFromBianaryLeft returns a upc code from number
     * @param UPCNumber in string format to convert
     * @return upc code equivalent of number entered
     */
    public static String getUPCIntegerFromBianaryLeft(String UPCNumber) {

        int uPCInt = Integer.parseInt(UPCNumber);

        switch (uPCInt) {
            case 1101:
                return "0";
            case 11001:
                return "1";
            case 10011:
                return "2";
            case 111101:
                return "3";
            case 100011:
                return "4";
            case 110001:
                return "5";
            case 101111:
                return "6";
            case 111011:
                return "7";
            case 110111:
                return "8";
            case 1011:
                return "9";
        }
        return "Error";
    }

    /**
     * getUPCIntegerFromBianaryLeft returns a upc code from number
     * @param UPCNumber in string format to convert
     * @return upc code equivalent of number entered
     */
    public static String getUPCIntegerFromBianaryRight(String UPCNumber) {

        int uPCInt = Integer.parseInt(UPCNumber);

        switch (uPCInt) {
            case 1110010:
                return "0";
            case 1100110:
                return "1";
            case 1101100:
                return "2";
            case 1000010:
                return "3";
            case 1011100:
                return "4";
            case 1001110:
                return "5";
            case 1010000:
                return "6";
            case 1000100:
                return "7";
            case 1001000:
                return "8";
            case 1110100:
                return "9";
        }
        return "Error";
    }


}


