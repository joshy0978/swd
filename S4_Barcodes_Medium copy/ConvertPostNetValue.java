

/**
 * ConvertPostNetValue is a classed used to encode/decode PostNet codes
 */
public class ConvertPostNetValue {

    /**
     * getPostnetBinaryValueFromNumber converts postnet number
     * to a binaryValue
     * @param number to convert
     * @return string value of number in binary value
     */

    public static String getPostnetBinaryValueFromNumber (String number) {

        int testInt = Integer.parseInt(number);
        String binaryValue = "";

        switch (testInt){
            case 0:
                binaryValue = "11000";
                break;
            case 1:
                binaryValue = "00011";
                break;
            case 2:
                binaryValue = "00101";
                break;
            case 3:
                binaryValue = "00110";
                break;
            case 4:
                binaryValue = "01001";
                break;
            case 5:
                binaryValue = "01010";
                break;
            case 6:
                binaryValue = "01100";
                break;
            case 7:
                binaryValue = "10001";
                break;
            case 8:
                binaryValue = "10010";
                break;
            case 9:
                binaryValue = "10100";
                break;
            case 11:
                binaryValue = "1";
                break;
            default:
                binaryValue = "Error";
                break;
        }

        return binaryValue;

    }

    /**
     *getPostNetLongShortValueFromNummber converts a Number to a
     * postnet long or short value
     * @param number to convert
     * @return String value of LongShort from number
     */
    public static String getPostNetLongShortValueFromNummber (String number) {

        int testInt = Integer.parseInt(number);
        String longShortValue = "";

        switch (testInt){
            case 0:
                longShortValue = "||...";
                break;
            case 1:
                longShortValue = "...||";
                break;
            case 2:
                longShortValue = "..|.|";
                break;
            case 3:
                longShortValue = "..||.";
                break;
            case 4:
                longShortValue = ".|..|";
                break;
            case 5:
                longShortValue = ".|.|.";
                break;
            case 6:
                longShortValue = ".||..";
                break;
            case 7:
                longShortValue = "|...|";
                break;
            case 8:
                longShortValue = "|..|.";
                break;
            case 9:
                longShortValue = "|.|..";
                break;
            case 11:
                longShortValue = "|";
                break;
            default:
                longShortValue = "Error";
                break;
        }

        return longShortValue;
    }

    /**
     * getPostNetNumberFromBinary converts Postnet number
     * from binary string
     * @param number String value of Binary to convert
     * @return string value of post net number from binary number
     */

    public static String getPostNetNumberFromBinary (String number) {

        int testInt = Integer.parseInt(number);
        String numericValue = "";

        switch (testInt){
            case 11000:
                numericValue = "0";
                break;
            case 11:
                numericValue = "1";
                break;
            case 101:
                numericValue = "2";
                break;
            case 110:
                numericValue = "3";
                break;
            case 1001:
                numericValue = "4";
                break;
            case 1010:
                numericValue = "5";
                break;
            case 1100:
                numericValue = "6";
                break;
            case 10001:
                numericValue = "7";
                break;
            case 10010:
                numericValue = "8";
                break;
            case 10100:
                numericValue = "9";
                break;
            default:
                numericValue = "Error";
                break;
        }

        return numericValue;


    }

    /**
     *getPostNetNumberFromLongShort converts a LongShort postnet number
     * to a postnet number
     * @param number String value of number to convert from Long to Short
     *               postnet number
     * @return the string value of the Postnet number from long short
     */
    public static String getPostNetNumberFromLongShort (String number) {

        int count = 0;
        String testString = "";

        for (int i = 0; i < number.length(); i++) {
            char testChar = number.charAt(i);
            String charToString = String.valueOf(testChar);
            if (charToString.equals("|")) {
                testString = testString + charToString;
                count = 1;

            }
            else if (count >= 1 && charToString.equals(".")){
                testString = testString + charToString;
            }
        }

        count = 0;

        return ConvertPostNetValue.getPostNetNumberFromLongShort(testString);

    }
}
