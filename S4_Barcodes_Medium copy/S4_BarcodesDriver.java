

import java.io.ByteArrayOutputStream;
import java.util.Scanner;

//import net.glxn.qrgen.QRCode;
//import net.glxn.qrgen.image.ImageType;

/**
 * Driver Class for S4_Barcodes_Medium.  Driver class instantiates a class
 * of S4_Barcodes_Medium.  Gets user import of codes to decode and encode.
 */
public class S4_BarcodesDriver {

    public static void main(String[] args) {



        //UPCA Encoder Example
        Scanner userInputEncoderUPCA = new Scanner(System.in);
        System.out.println("Enter UPC product code to encode:");
        String upcaStringToEncode = userInputEncoderUPCA.next();


        S4_Barcodes_Medium testBarCode = new S4_Barcodes_Medium();
        testBarCode.setUserUPCodeToEncode(upcaStringToEncode);
        String upcBarcode = testBarCode.encodeUPCACode();
        System.out.println(upcBarcode);

        //Decoder UPCA Example
        Scanner userInputDecoderUPCA = new Scanner(System.in);
        System.out.println("Enter UPC code to decode:");
        String upcaStringToDecode = userInputEncoderUPCA.next();


        testBarCode.setUserUPCodeToDecode(upcaStringToDecode);
        System.out.println("Check digit - " + testBarCode.getUPCACheckBitToDecode() + ", ok");
        System.out.println("Product Code - " + testBarCode.decodeUPCACode());



        //Postnet Encoder Example

        Scanner userInputEncoderPostnet = new Scanner(System.in);
        System.out.println("\nEnter zip code:");
        String postNetStringToEncode = userInputEncoderPostnet.next();

        testBarCode.setUserPostNetCodeToEncode(postNetStringToEncode);
        testBarCode.encodePostnetCode();

        //Postnet Decoder Example

        Scanner userInputDecoderPostnet = new Scanner(System.in);
        System.out.println("Enter Postnet code to decode");
        String postNetStringToDecode = userInputDecoderPostnet.next();

        testBarCode.setUserPostNetCodeToDecode(postNetStringToDecode);
        testBarCode.decodeBinaryPostnetCode();








    }
}
