

import javax.swing.*;

/**
 * Created by joshuataylor on 9/22/15.
 */
public class S15_ArabicToRomanGUI_MediumDriver {

    /**
     *Drives class for S15_ArabicToRomanGUI_Medium.  When class is ran an Instance of S15_ArabicToRomanGUI_Medium is created lanches the object
     * reference converter.
     * @param args
     */

    public static void main(String[] args) {

        S15_ArabicToRomanGUI_Medium converter = new S15_ArabicToRomanGUI_Medium();
    }

}
