/**
 * ArabicToRomanGUI_Enum is an Enumeration class.  This class holds Roman Numerals and there corresponding
 * constants.
 */
public enum ArabicToRomanGUI_Enum {
    THOUSAND (1000, "M"),
    FIVEHUNDRED (500, "D"),
    HUNDRED (100, "C"),
    FIFTY (50, "L"),
    TEN (10, "X"),
    FIVE (5, "V"),
    ONE (1, "I");

    private final int arabicNumber;
    private final String romanNumeral;

    ArabicToRomanGUI_Enum(int arabicNumber, String romanNumeral) {
        this.arabicNumber = arabicNumber;
        this.romanNumeral = romanNumeral;
    }

    /**
     *
     * @return a String of the class variable romanNumeral
     */
    public String getRomanNumeral() {
        return romanNumeral;
    }

    /**
     *
     * @return an integer of the class variable arabicNumber
     */
    public int getArabicNumber() {
        return arabicNumber;
    }

    /**
     *
     * @param valueToConvert a String that is the Roman number that is being converted.
     * @return the String value of the Arabic Value
     */
    public static String convertRomanToArabic(String valueToConvert) {
        int total = 0;

        char[] characters = valueToConvert.toCharArray();
        int[] values = new int[characters.length];
        for (int i = 0; i < characters.length; i++) {

            System.out.println(values[i]);
        }
        for(int i = 0; i < characters.length; i++) {
            for (ArabicToRomanGUI_Enum valueToCompare: ArabicToRomanGUI_Enum.values())
                if (valueToCompare.romanNumeral.equals(String.valueOf(characters[i]))) {
                    values[i] = valueToCompare.getArabicNumber();
                    System.out.println(values[i]);
                }
        }

        int i = values.length - 1;

        while (i >= 0) {
            int currentValue =  values[i];

            if ((i -1) >= 0) {
                if ( values[i-1] < currentValue){
                    total += currentValue - values[i-1];
                    i -= 2;
                }  else{
                    total += values[i];
                    i--;
                }

            } else {
                total += values[i];
                i--;
            }
        }



        String convertedNumber = String.valueOf(total);
        return convertedNumber;
    }
}

