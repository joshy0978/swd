

/**
 * Created by joshuataylor on 10/5/15.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by joshuataylor on 9/22/15.
 */
public class S15_ArabicToRomanGUI_Medium {

    private JFrame frame;
    private JTextArea arabicTextArea;
    private JTextArea romanTextArea;
    private JButton convertButton;

    /**
     * Constructor that creates a new JFrame and populates itself with 2 JTextArea and 1 JButton.
     */

    S15_ArabicToRomanGUI_Medium() {
        //create the JFrame and components


        frame = new JFrame("Roman Numeral to Arabic Number Converter");
        frame.setSize(500, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout());


        //JTextArea
        romanTextArea = new JTextArea("Enter Roman Numeral To Convert", 3, 20);
        arabicTextArea = new JTextArea("", 3, 20);

        //JButton
        convertButton = new JButton("Convert");


        frame.add(romanTextArea);
        frame.add(arabicTextArea);
        frame.add(convertButton);
        frame.setVisible(true);

        event mouseClick = new event();
        convertButton.addActionListener(mouseClick);
    }

    /**
     *  The listener interface for reciving actions from the user's mouse click.
     */
    public class event implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String textFromRoamnTextArea = romanTextArea.getText().toUpperCase();
            String romanNumeralFromArabicTextAreaValue = ArabicToRomanGUI_Enum.convertRomanToArabic(textFromRoamnTextArea);
            arabicTextArea.setText(romanNumeralFromArabicTextAreaValue);
        }
    }

}
