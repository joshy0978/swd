import java.util.Arrays;


/**
 * MergeSort is a class that performs a merge sort on an array.  The class contains 
 * 2 methods: mergeSort and mergeAndSortLeftWithRightArray.  mergesort is a recursive function.
 * @author joshuataylor
 *
 */
public class MergeSort {
    
    /**
     * mergeSort is a static method that takes the parameter int[] array.  Then sorts the array using 
     * recussion.
     * @param int[] arrayToSort
     * @return int[]
     */
    public static int[] mergeSort (int[] arrayToSort) {

        //create a new array from inputed arrayToSort lenght
        int[] sortedArray = new int[arrayToSort.length];

        //copy values from arrayToSort to sortedArray
        for (int i = 0; i < arrayToSort.length; i++) {
            int value = arrayToSort[i];
            sortedArray[i] = value;
        }

        //checks for base case
        //If the array is n
        if (arrayToSort.length > 1) {

            //example if the length is 3
            //the return value of 3/2 = 1
            int middleOfArray = arrayToSort.length/2;

            //setup size of array
            int leftSideArraySizeIntValue = middleOfArray;
            int rightSideArraySizeIntValue = sortedArray.length - middleOfArray;

            //Create an array to hold left handed values
            int[] leftSideArray = new int[leftSideArraySizeIntValue];

            //Populate left handed array with values
            for (int i = 0; i < leftSideArray.length; i++) {
                int value = arrayToSort[i];
                leftSideArray[i] = value;
            }
            System.out.println("LeftSideArray: " + Arrays.toString(leftSideArray));


            //Create an array to hold right handed values
            int[] rightSideArray = new int[rightSideArraySizeIntValue];

            //Populate right handed array with values
            int locationOfEndOfArrayToSort = sortedArray.length - 1;
            for (int j = 0; j < rightSideArray.length; j++) {
                int value = arrayToSort[locationOfEndOfArrayToSort - j];
                rightSideArray[j] = value;
                System.out.println("rightSideArray value: " + rightSideArray[j]);
            }
            System.out.println("RightSideArray: " + Arrays.toString(rightSideArray) );

            mergeSort(leftSideArray);
            mergeSort(rightSideArray);


            mergeAndSortLeftWithRightArray(leftSideArray, rightSideArray, arrayToSort);

            return arrayToSort;
        }

        return arrayToSort;


    }


    /**
     * mergeAndSortLeftWithRightArray is a static method has three parameters each being a int[].  This method takes two arrays, sorts
     * them then joins them back together.
     * @param leftSideOfArray
     * @param rightSideOfArray
     * @param arrayToStoreSortedArray
     */
    public static void mergeAndSortLeftWithRightArray(int[] leftSideOfArray, int[] rightSideOfArray, int[] arrayToStoreSortedArray) {
        //fun part
        System.out.println("mergeandsort geting called");

        //setup local variables

        //get the size of each array because they could be different sizes
        //and
        //add counters to store value into arrayTosToreStoredArray
        //int locationToStoreValueInArrayToStoreSortedArray = 0;
        //positiont of of right and left array
        int leftSideArraySizeIntValue = leftSideOfArray.length;
        int leftPostition = 0;

        int rightSideArraySizeIntValue = rightSideOfArray.length;
        int rightPostiong = 0;

        //postion of array to store value
        int arrayToStoreSortedArrayIntValue = 0;

        //Loop through values and compare and put bigger number in array
        //IF THE ARRAYS ARE NOT THE SAME SIZE THE LEFT SIDE WILL ALWAYS BE BIGGER!!!!!!
        while ((leftPostition < leftSideArraySizeIntValue) & (rightPostiong < rightSideArraySizeIntValue)) {

            //if leftside bigger
            if (leftSideOfArray[leftPostition] <= rightSideOfArray[rightPostiong]) {
                arrayToStoreSortedArray[arrayToStoreSortedArrayIntValue] = leftSideOfArray[leftPostition];
                arrayToStoreSortedArrayIntValue++;
                leftPostition++;
            } else {
                arrayToStoreSortedArray[arrayToStoreSortedArrayIntValue] = rightSideOfArray[rightPostiong];
                arrayToStoreSortedArrayIntValue++;
                rightPostiong++;
            }

        }

        //If values are leftover in leftside
        //Add values to leftside array to arrayToSToreSortedArray
        for (int i = leftPostition; i < leftSideArraySizeIntValue; i++) {
        	arrayToStoreSortedArray[arrayToStoreSortedArrayIntValue] = leftSideOfArray[i];
            arrayToStoreSortedArrayIntValue++;
        }

        //If values are leftover in rightside
        //Add values to rightside array to arrayToSToreSortedArray
        for (int j = rightPostiong; j < rightSideArraySizeIntValue; j++) {
        	arrayToStoreSortedArray[arrayToStoreSortedArrayIntValue] = rightSideOfArray[j];
            arrayToStoreSortedArrayIntValue++;
        }
        


    }




}
