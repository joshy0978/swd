/**
 * RandomArrayGenerator is a class with one 
 * static method called randomArray() that creates 
 * a random array with 1000 elements
 * @author joshuataylor
 *
 */
public class RandomArrayGenerator {

	/**
	 * randomArray() is a static method that returns an array containing 1000 random integers
	 * @return int[] containing 1000 elements
	 */
	public static int[] randomArray() {
		
		int [] randomNumbers = new int[1000];
		
		for (int i = 0; i < 1000; i++){
			int randomNumber = (int) (1000 * Math.random());
			randomNumbers[i] = randomNumber;
		}
		
		return randomNumbers;
		
	}
}
