
/**
 * MergeSortDriver is a class that instantiates an array 
 * that contains 1000 random intergers and then calls MergeSort on the integer array of 1000
 * random ints.  Then saves the newly sorted array in a new array named sortedArray.
 * @author joshuataylor
 *
 */
public class MergeSortDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] randomNumbers = RandomArrayGenerator.randomArray();
		
//		for( int i = 0; i < randomNumbers.length; i++) {
//			System.out.println(randomNumbers[i]);
//		}
		
		
		int[] sortedArray = MergeSort.mergeSort(randomNumbers);
		
		for( int j = 0; j < randomNumbers.length; j++) {
			System.out.println(sortedArray[j]);
		}
		
	}

}
