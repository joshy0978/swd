package com.example.stmpeclispe;

import java.io.Serializable;

/**
 * Question Object will hold all the information about each question and provides methods to
 * retrieve the information.
 *
 * Created by Jacob on 12/4/15.
 */
public class QuestionObject implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2331600733929050723L;
    private String question;
    private String explanation;
    private String[] answers;
    private String imageFileName;

    public String toString(){
        return question;
    }

    public QuestionObject(String question, String[] answers) {
        this.question = question;
        this.answers = answers;
        this.explanation = null;
        this.explanation = null;
    }

    public QuestionObject(String question, String[] answers, String explanation) {
        this.question = question;
        this.answers = answers;
        this.explanation = explanation;
        this.imageFileName = null;
    }

    public QuestionObject(String question, String[] answers, String explanation, String image) {
        this.question = question;
        this.answers = answers;
        this.explanation = explanation;
        this.imageFileName = image;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public String[] getAnswers() {
        return answers;
    }

    public void setAnswers(String[] answers) {
        this.answers = answers;
    }

    public String getImage() {
        return imageFileName;
    }

    public void setImage(String image) {
        this.imageFileName = image;
    }

    public void printAnswers(){
        for(String line: answers){
            if(line != null){
                System.out.println(line);
            }
        }
    }

}