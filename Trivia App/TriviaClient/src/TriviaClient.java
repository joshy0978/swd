import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.example.stmpeclispe.QuestionObject;
import com.example.stmpeclispe.UserProfile;

public class TriviaClient {
	private Socket serverConnection;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	private String gameHost;
	private UserProfile myProfile;
	private boolean isLoggedIn;

	public TriviaClient(String host) {
		gameHost = host;
		myProfile = null;
		isLoggedIn = false;
	}

	public static void main(String[] args) {
		TriviaClient app = new TriviaClient("172.17.51.248");
		//TriviaClient app = new TriviaClient("127.0.0.1");
		app.connectClient();
	}

	public void connectClient() {
		try {
			serverConnection = new Socket(InetAddress.getByName(gameHost), 23495);

			output = new ObjectOutputStream(serverConnection.getOutputStream());
			output.flush();
			input = new ObjectInputStream(serverConnection.getInputStream());
			System.out.println("client success");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("executing client");

		Scanner consoleInput = new Scanner(System.in);
		System.out.println("Enter username and password with enter inbetween");
		String username = consoleInput.nextLine();
		String userpass = consoleInput.nextLine();
		try {
			output.writeObject("Existing User");
			output.flush();
			output.writeObject(username);
			output.flush();
			output.writeObject(userpass);
		} catch (IOException e) {
			e.printStackTrace();
		}
		while (true) {
			try {
				Object newData = input.readObject();
				if (newData instanceof String) {
					String inputString = (String) newData;
					System.out.println(inputString);
					 processMessage(inputString);
				} else if (newData instanceof UserProfile) {
					myProfile = (UserProfile) newData;
					System.out.println(myProfile);
					break;
				}
			}catch(EOFException e){
				//disconnect server
				break;
			}
			catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
		try{
			output.writeObject("Play 2 Player Game");
			output.flush();
			String serverResponse = (String) input.readObject();
			System.out.println(serverResponse);
			serverResponse = (String) input.readObject();
			System.out.println(serverResponse);
			output.writeObject("Sending Score");
			output.flush();
			output.writeObject(new Integer(5));
			output.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		//consoleInput.close();
	}

	private void processMessage(String message) {
		System.out.println(message);
		if (message.equals("Log In")) {
			Scanner consoleInput = new Scanner(System.in);
			System.out.println("Enter username and password with a space");
			String userpass = consoleInput.nextLine();
			try {
				output.writeObject("Existing User");
				output.flush();
				output.writeObject(userpass);
				output.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			String logInResponse = "";

			try {
				Object newData = input.readObject();
				if (newData instanceof String) {
					logInResponse = (String) newData;

					if (logInResponse.equals("Success")) {
						System.out.println("Logged in!");
						output.writeObject("Play 2 Player Game");
					} else if (logInResponse.equals("Failure")) {
						System.out.println("Failure");
					}
				}
			} catch (Exception e) {

			}
		}

	}

}