
public class UserProfile {

	private final String userName;
	private final String password;
	private int overallScore;
	private int numberOfWins;
	private int numberOfLosses;

	public UserProfile(String username, String password) {
		this.userName = username;
		this.password = password;
		this.overallScore=0;
		this.numberOfWins=0;
		this.numberOfLosses=0;
	}

	public UserProfile(String username, String password, int wins, int losses,int overallScore) {
		this.userName = username;
		this.password = password;
		this.numberOfWins=wins;
		this.numberOfLosses = losses;
		this.overallScore = overallScore;
	}

	public String getUserName() {
		return userName;
	}


	public String getPassWord() {
		return password;
	}

	public int getOverallScore() {
		return overallScore;
	}

	public void addToOverallScore(int scoreToAdd) {
		overallScore+=scoreToAdd;
	}

	public int getNumberOfWins() {
		return numberOfWins;
	}

	public void incrementNumberOfWins() {
		this.numberOfWins++;
	}

	public int getNumberOfLosses() {
		return numberOfLosses;
	}

	public void incrementNumberOfLosses() {
		this.numberOfLosses++;
	}


}

