package com.example.stmpeclispe;

import java.util.HashMap;

public class ImageMapReturner {
	HashMap<String, String> imageMap;
	public String returnImage(String key){
		return imageMap.get(key);
	}
	ImageMapReturner() { 
		imageMap = new HashMap<>();
		imageMap.put("1.png", "onep");
		imageMap.put("2.png", "twop");
		imageMap.put("3.png", "threep");
		imageMap.put("4.png", "fourp");
		imageMap.put("5.png", "fivep");
		imageMap.put("6.gif", "sixg");
		imageMap.put("7.png", "sevenp");
		imageMap.put("8.png", "eightp");
		imageMap.put("9.png", "ninep");
		imageMap.put("10.png", "tenp");
		imageMap.put("11.jpg", "elevenj");
		imageMap.put("12.gif", "twelveg");
		imageMap.put("13.png", "thirteenp");
		imageMap.put("14.png", "fourteenp");
		imageMap.put("15.png", "fifteenp");
		imageMap.put("16.png", "sixteenp");
		imageMap.put("17.png", "seventeenp");
		imageMap.put("18.png", "eighteenp");
		imageMap.put("20.png", "twentyp");
		imageMap.put("21.png", "twentyonep");
		imageMap.put("22.png", "twentytwop");
		imageMap.put("23.png", "twentythreep");
		imageMap.put("24.png", "twentyfourp");
		imageMap.put("25.png", "twentyfivep");
		imageMap.put("26.png", "twentysixp");
		imageMap.put("27.png", "twentysevenp");
		imageMap.put("28.png", "twentyeightp");
		imageMap.put("29.png", "twentyninep");
		imageMap.put("30.png", "thirtyp");
		imageMap.put("31.png", "thirtyonep");
		imageMap.put("32.png", "thirtytwop");
		imageMap.put("33.png", "thirtythreep");
		imageMap.put("34.png", "thirtyfourp");
		imageMap.put("35.png", "thirtyfivep");
		imageMap.put("36.png", "thirtysixp");
		imageMap.put("37.png", "thirtysevenp");
		imageMap.put("38.png", "thirtyeightp");
		imageMap.put("39.png", "thirtyninep");
		imageMap.put("40.gif", "fortyg");
		imageMap.put("41.png", "fortyonep");
		imageMap.put("42.png", "fortytwop");
		imageMap.put("43.png", "fortythreep");
		imageMap.put("44.png", "fortyfourp");
		imageMap.put("45.png", "fortyfivep");
		imageMap.put("46.gif", "fortysixg");
		imageMap.put("47.gif", "fortyseveng");
	}
}