import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;
import java.awt.BorderLayout;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import com.example.stmpeclispe.QuestionObject;
import com.example.stmpeclispe.UserProfile;

public class TriviaServer extends JFrame {
	private ExecutorService runGame;
	private ConcurrentHashMap<String, UserProfile> userList;
	private ServerSocket server;
	private TriviaGameLobby gameLobby;
	private QuestionParser questionParser;
	private List<QuestionObject> questionList;
	private UserProfileManager userProfileTool;
	private JTextArea outputArea;

	public static void main(String[] args) {
		TriviaServer app = new TriviaServer();
		app.runServer();
	}

	public TriviaServer() {
		super("STUMPED TRIVIA SERVER");
		runGame = Executors.newFixedThreadPool(10);
		userList = new ConcurrentHashMap<String, UserProfile>();
		userProfileTool = new UserProfileManager("UserProfile.xml");
		UserProfile initializeUser;
		for (int i = 0; i < userProfileTool.getNumberOfUsers(); i++) {
			initializeUser = userProfileTool.getUser(Integer.toString(i));
			userList.put(initializeUser.getUserName(), initializeUser);
			//System.out.println(initializeUser);
			//System.out.println("");
		}
		gameLobby = new TriviaGameLobby();
		questionParser = new QuestionParser("questions.xml");
		ArrayList<QuestionObject> tempList = questionParser.parseQuestionObjects();
		questionList = Collections.synchronizedList(tempList);
		for(QuestionObject lister : tempList){
			System.out.println(lister.getImage());
		}

		try {
			server = new ServerSocket(23495, 50);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		outputArea = new JTextArea();
		add(outputArea, BorderLayout.CENTER);
		outputArea.setText("Server awaiting connections\n");

		setSize(600, 600);
		setVisible(true);
	}

	/**
	 * displays message on the server GUI
	 * 
	 * @param messageToDisplay
	 */
	public void displayMessage(final String messageToDisplay) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				outputArea.append(messageToDisplay);
			}
		});
	}

	public void runServer() {

		runGame.execute(gameLobby);
		while (true) {
			try {
				TriviaPlayer newPlayer = new TriviaPlayer(server.accept());
				runGame.execute(newPlayer);
			} catch (IOException e) {
				System.err.println("error accepting connection");
				e.printStackTrace();
			}
		}

	}

	/**
	 * obtains 10 QuestionObjects and returns an Array of them. synchronized
	 * around the atomic operation where the list is shuffled and the objects
	 * are assigned to the array
	 * 
	 * @return array of Questions
	 */
	public QuestionObject[] obtainQuestionsForGame() {
		QuestionObject[] returnArray = new QuestionObject[10];
		synchronized (questionList) {
			Collections.shuffle(questionList);
//			for (int i = 0; i < 10; i++) {	
//				returnArray[i] = questionList.get(i);
//			}
			int i=0;
			for(QuestionObject iter : questionList){
				if(i>9){
					break;
				}
				else if(iter.getImage()!=null){
					returnArray[i]=iter;
					i++;
				}
			}
		}
		return returnArray;
	}

	/**
	 * method used to attempt login of a user. returns false if the user exists
	 * and the password is correct returns false otherwise
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean attemptLogin(String username, String password) {
		UserProfile user = userList.get(username);
		if (user == null) {
			return false;
		} else if (user.isLoggedIn()) {
			return false;
		} else if (user.getPassWord().equals(password)) {
			user.logIn();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * attempts to create new user, returns true if new user was created, false
	 * if the username already exists or the password was of invalid length(
	 * less than or equal to 0)
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean createNewUser(String username, String password) {
		if (userList.get(username) != null) {
			return false;
		} else if (password.length() <= 0) {
			return false;
		} else {
			UserProfile newUser = new UserProfile(username, password);
			userList.put(username, newUser);
			addToUserXMLFile(newUser);
			return true;
		}

	}
	// public void terminatePlayer(TriviaPlayer user){
	// SwingUtilities.invokeLater(new Runnable() {
	// public void run() {
	//
	// }
	// });
	// }

	/**
	 * method that is as of now unimplemented that will be implemented once the
	 * XML is introduced
	 * 
	 * @param newUser
	 *            user to be added
	 */
	public void addToUserXMLFile(UserProfile newUser) {
		synchronized (userProfileTool) {
			userProfileTool.addUser(newUser.getUserName(), newUser.getPassWord());
		}
	}

	public void updateUserXML(UserProfile thisUser) {
		synchronized (userProfileTool) {
			userProfileTool.updateUser(thisUser);
		}

	}

	private class TriviaGameLobby implements Runnable {
		/**
		 * queue of players waiting. currently initialized in constructor with
		 * max num of 8 and fairness policy.
		 */
		private ArrayBlockingQueue<TriviaPlayer> twoPlayerGameWaitingQueue;
		private ArrayBlockingQueue<TriviaPlayer> fourPlayerGameWaitingQueue;

		public TriviaGameLobby() {
			twoPlayerGameWaitingQueue = new ArrayBlockingQueue<TriviaPlayer>(80, true);
			fourPlayerGameWaitingQueue = new ArrayBlockingQueue<TriviaPlayer>(80, true);
		}

		public void run() {
			while (true) {
				synchronized (twoPlayerGameWaitingQueue) {
					if (twoPlayerGameWaitingQueue.size() >= 2) {
						TriviaGame newGame = new TriviaGame(twoPlayerGameWaitingQueue.poll(),
								twoPlayerGameWaitingQueue.poll());
						System.out.println("creating new Game!");
						runGame.execute(newGame);
					}
				}
				synchronized (fourPlayerGameWaitingQueue) {
					if (fourPlayerGameWaitingQueue.size() >= 4) {
						TriviaGame newGame = new TriviaGame(fourPlayerGameWaitingQueue.poll(),
								fourPlayerGameWaitingQueue.poll(), fourPlayerGameWaitingQueue.poll(),
								fourPlayerGameWaitingQueue.poll());
						runGame.execute(newGame);
					}
				}
			}
		}

		public void add2PersonPlayer(TriviaPlayer triviaPlayer) throws InterruptedException {
			System.out.println("adding player");
			try {
				twoPlayerGameWaitingQueue.put(triviaPlayer);
				System.out.println("successful add");
			} catch (InterruptedException e) {
				throw e;
			}

		}

		public void add4PersonPlayer(TriviaPlayer triviaPlayer) throws InterruptedException {
			try {
				fourPlayerGameWaitingQueue.put(triviaPlayer);
			} catch (InterruptedException e) {
				throw e;
			}
		}
	}

	/**
	 * Comparator map sorting code found from following link:
	 * http://webcache.googleusercontent.com/search?q=cache:http://www.
	 * programcreek.com/2013/03/java-sort-map-by-value/
	 * 
	 * @author keeganshay
	 *
	 */
	private class TriviaGame implements Runnable {
		TriviaPlayer[] players;
		QuestionObject[] gameQuestionArray;
		private String gameType;

		public TriviaGame(TriviaPlayer player1, TriviaPlayer player2) {
			players = new TriviaPlayer[2];
			gameQuestionArray = new QuestionObject[10];
			players[0] = player1;
			players[1] = player2;
			gameType = "Two Player Results";
		}

		public TriviaGame(TriviaPlayer player1, TriviaPlayer player2, TriviaPlayer player3, TriviaPlayer player4) {
			players = new TriviaPlayer[4];
			players[0] = player1;
			players[1] = player2;
			players[2] = player3;
			players[3] = player4;
			gameType = "Four Player Results";
		}

		public boolean playersFinished() {
			for (int i = 0; i < players.length; i++) {
				if (players[i].playingGame) {
					return false;
				}
			}
			return true;
		}

		public boolean playersConnected() {
			for (int i = 0; i < players.length; i++) {
				synchronized (players[i]) {
					if (!players[i].isLoggedIn) {
						return false;
					}
				}
			}
			return true;
		}

		public void run() {
			System.out.println("GAME STARTED!");
			gameQuestionArray = TriviaServer.this.obtainQuestionsForGame();
			for (int i = 0; i < players.length; i++) {
				players[i].beginGame(gameQuestionArray);
				players[i].sendQuestionsToPlayer();
				System.out.println("questions sent");
			}
			while (!playersFinished()) {

			}
			//if (playersConnected()) {
				System.out.println("Players Finished!");
				HashMap<String, Integer> resultsMap = new HashMap<>();

				for (int i = 0; i < players.length; i++) {
					int score = players[i].getGameScore();
					resultsMap.put(players[i].getPlayerUsername(), score);
				}
				ValueComparator sort = new ValueComparator(resultsMap);
				TreeMap<String, Integer> sortedResultsMap = new TreeMap<>(sort);
				sortedResultsMap.putAll(resultsMap);
				TreeMap<String, Integer> sortedResultsMap2 = new TreeMap<>(sort);
				sortedResultsMap2.putAll(resultsMap);
				String[] sendResultsString = new String[players.length + 1];
				System.out.println(sortedResultsMap.toString());
				sendResultsString[0] = gameType;
				int winningScore = 0;
				for (int i = 1; i < players.length + 1; i++) {
					String username = sortedResultsMap.firstEntry().getKey();
					int playerScore = sortedResultsMap.pollFirstEntry().getValue();
					sendResultsString[i] = username + ":   " + playerScore + " points";
					if (playerScore > winningScore) {
						winningScore = playerScore;
					}
				}
				for (int i = 0; i < players.length; i++) {
					String firstLine = "";
					if (players[i].getGameScore() == winningScore) {
						firstLine = "You Won";
					} else {
						firstLine = "You Lost";
					}
					System.out.println(players[i].getPlayerUsername() + "  " + firstLine);
					String[] finalSendString = new String[sendResultsString.length + 1];
					finalSendString[0] = firstLine;
					for (int j = 1; j < finalSendString.length; j++) {
						finalSendString[j] = sendResultsString[j - 1];
					}
					System.out.println(Arrays.toString(finalSendString));
					players[i].receiveGameResults(finalSendString);
				}
			/*} else {
				System.out.println("error results for game");
				String[] errorResults = new String[2];
				errorResults[0] = "Error";
				errorResults[1] = "Error, opponent(s) disconnected";
				for (int i = 0; i < players.length; i++) {
					players[i].receiveGameResults(errorResults);
				}
			}*/

		}

		private class ValueComparator implements Comparator<String> {

			Map<String, Integer> map;

			public ValueComparator(Map<String, Integer> base) {
				this.map = base;
			}

			public int compare(String a, String b) {
				if (map.get(a) >= map.get(b)) {
					return -1;
				} else {
					return 1;
				}
			}
		}
	}

	private class TriviaPlayer implements Runnable {
		/**
		 * socket used to connect to the client
		 */
		private final Socket connection;
		/**
		 * input stream from the client
		 */
		private ObjectInputStream input;
		/**
		 * output stream to the client
		 */
		private ObjectOutputStream output;
		/**
		 * boolean denoting wether the player has Logged in or not
		 */
		private boolean isLoggedIn;
		/**
		 * boolean denoting wether the user is waiting to play a game
		 */
		private boolean inGameQueue;
		/**
		 * denotes wether or not the user is in the game
		 */
		private boolean playingGame;
		/**
		 * object containing a reference to the current UserProfile
		 */
		private UserProfile thisUser;
		private QuestionObject[] gameQuestionArray;
		private Boolean obtainedGameResults;
		private int currentGameScore;
		private String[] gameResults;
		private boolean goodSocket;

		public TriviaPlayer(Socket connection) {
			this.connection = connection;
			// try{
			// this.connection.bind(new
			// InetSocketAddress("64.134.197.121",23495));
			// }catch(IOException e){
			//
			// }
			thisUser = null;
			inGameQueue = false;
			isLoggedIn = false;
			playingGame = false;
			gameQuestionArray = null;
			obtainedGameResults = false;
			currentGameScore = 0;
			gameResults = null;
			goodSocket = true;

		}

		public Integer getGameScore() {
			return currentGameScore;
		}

		public void receiveGameResults(String[] results) {
			gameResults = results;
			synchronized (obtainedGameResults) {
				this.obtainedGameResults = true;
			}
		}

		public void run() {
			try {
				output = new ObjectOutputStream(connection.getOutputStream());
				output.flush();
				input = new ObjectInputStream(connection.getInputStream());
				System.out.println("server success");
			} catch (IOException e) {
				System.err.println("Error establishing input and/or output streams");
				e.printStackTrace();
			}

			while (!isLoggedIn && goodSocket) {
				try {
					// output.writeObject("Log In");
					// output.flush();
					String nextMessage = "";
					Object newInputData = input.readObject();
					if (newInputData instanceof String) {
						nextMessage = (String) newInputData;
						System.out.println(nextMessage);
						processMessage(nextMessage);
					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}

				while (!isLoggedIn && goodSocket) {
					String nextMessage = getNextOutputString();
					processMessage(nextMessage);
				}
				while (isLoggedIn && goodSocket) {
					String nextMessage = getNextOutputString();
					processMessage(nextMessage);
					/*
					 * if(playingGame){ this.sendQuestionsToPlayer();
					 * nextMessage = getNextOutputString();
					 * processMessage(nextMessage); }
					 */
				}
			}
			System.out.println("player no longer running");
		}

		public void beginGame(QuestionObject[] gameQuestionArray) {
			this.inGameQueue = false;
			this.playingGame = true;
			this.obtainedGameResults = false;
			this.gameQuestionArray = gameQuestionArray;
		}

		public void sendQuestionsToPlayer() {
			try {

				synchronized (gameQuestionArray) {
					output.writeObject("Sending Questions");
					output.flush();
					for (int i = 0; i < 10; i++) {
						output.writeObject(gameQuestionArray[i]);
						output.flush();
						System.out.println(gameQuestionArray[i].getImage());
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		public String getPlayerUsername() {
			if (thisUser != null) {
				return thisUser.getUserName();
			} else {
				return "";
			}
		}

		/**
		 * function called to get the next string from the output. basically
		 * saves the reusal of try catch surrounding the read object and
		 * determining if the object read is a string. ONLY call this function
		 * when you are certain the next object passed will be a string (as
		 * coded in the client)
		 * 
		 * @return
		 */
		private synchronized String getNextOutputString() {
			try {
				Object nextObject = input.readObject();
				if (nextObject instanceof String) {
					System.out.println(((String) nextObject));
					return (String) nextObject;
				}
			} catch (SocketException e) {
				synchronized (this) {
					this.isLoggedIn = false;
					thisUser.logOut();
				}
				System.out.println("Socket reset");
				goodSocket = false;
			} catch (EOFException e) {
				synchronized (this) {
					this.isLoggedIn = false;
					thisUser.logOut();
				}
			} catch(NullPointerException e){
				synchronized (this) {
					this.isLoggedIn = false;
					thisUser.logOut();
				}
			}catch (ClassNotFoundException e) {
				synchronized (this) {
					this.isLoggedIn = false;
					thisUser.logOut();
				}
				e.printStackTrace();
			} catch (IOException e) {
				synchronized (this) {
					this.isLoggedIn = false;
					thisUser.logOut();
				}
				e.printStackTrace();
			}
			return "";
		}

		/**
		 * processing Strings and taking action. if you need to define a new
		 * String to send from the client, define its appropriate response
		 * action here, unless the string passed in above is already defined
		 * here, then you must add it in the if else if statement that defines
		 * the parent String message equality
		 * 
		 * @param message
		 */
		public void processMessage(String message) {
			if (message.equals("Existing User")) {
				// String[] loginInfo = getNextOutputString().split(" ");
				String username = getNextOutputString();
				String password = getNextOutputString();
				System.out.println(username + " " + password);
				try {
					if (TriviaServer.this.attemptLogin(username, password)) {
						output.writeObject("Success");
						output.flush();
						TriviaServer.this.displayMessage("Success, logged in " + username);
						this.thisUser = TriviaServer.this.userList.get(username);
						isLoggedIn = true;
						thisUser.logIn();
						output.writeObject(thisUser);
						output.flush();
					} else {
						output.writeObject("Failure");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (message.equals("New User")) {
				String username = getNextOutputString();
				String password = getNextOutputString();
				System.out.println(username + " " + password);
				try {
					if (createNewUser(username, password)) {
						output.writeObject("Success");
						output.flush();
						TriviaServer.this.displayMessage("Success,created and logged in " + username);

						thisUser = TriviaServer.this.userList.get(username);
						System.out.print(thisUser);
						output.writeObject(thisUser);
						output.flush();
					} else {
						output.writeObject("Failure");
						output.flush();
					}
				} catch (IOException e) {

				}
			} else if (message.equals("Play 2 Player Game")) {
				while (!inGameQueue) {
					try {
						TriviaServer.this.displayMessage(thisUser.getUserName() + "adding to 2 player game");
						TriviaServer.this.gameLobby.add2PersonPlayer(this);
						inGameQueue = true;
						output.writeObject("Waiting For Game");
						output.flush();
					} catch (InterruptedException e) {
						// try {
						// //output.writeObject("Waiting For Game");
						// //output.flush();
						// } catch (IOException e1) {
						// e1.printStackTrace();
						// }
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			} else if (message.equals("Play 4 Player Game")) {
				TriviaServer.this.displayMessage(thisUser.getUserName()+" adding to 4 player Game");
				while (!inGameQueue) {
					try {
						TriviaServer.this.gameLobby.add4PersonPlayer(this);
						inGameQueue = true;
						output.writeObject("Waiting For Game");
						output.flush();
					} catch (InterruptedException e) {
						// try {
						// //output.writeObject("Waiting For Game");
						// //output.flush();
						// } catch (IOException e1) {
						// e1.printStackTrace();
						// }
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} else if (message.equals("Sending Score")) {

				try {
					TriviaServer.this.displayMessage("Score being processed in server for "+thisUser.getUserName());
					currentGameScore = (Integer) input.readObject();
					System.out.println("Integer Score is: " + currentGameScore);
					thisUser.addToOverallScore(currentGameScore);
					playingGame = false;
					System.out.println(thisUser.getUserName() + "   Score:  " + currentGameScore);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				while (!obtainedGameResults()) {

				}
				System.out.println("received game results!");
				if (gameResults[0].equals("You Won")) {
					thisUser.incrementNumberOfWins();
				} else if (gameResults[0].equals("You Lost")) {
					thisUser.incrementNumberOfLosses();
				}
				TriviaServer.this.updateUserXML(thisUser);
				for (int i = 0; i < gameResults.length; i++) {

					try {
						output.writeObject(gameResults[i]);
						output.flush();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				gameResults = null;
			}
		}

		private boolean obtainedGameResults() {
			boolean returnval;
			synchronized (obtainedGameResults) {
				returnval = obtainedGameResults;
			}
			return returnval;

		}

	}

}