package com.example.joshkeeganjake.stmpedappfinal;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by joshuataylor on 12/7/15.
 */
public class ClientNetworkConnection {private static ClientNetworkConnection myInstance;
    public static final String hostIP = "172.17.51.248";

    private Socket serverConnection;
    private ObjectInputStream input;
    private ObjectOutputStream output;
    private String gameHost;

    public UserProfile getMyProfile() {
        return myProfile;
    }

    private UserProfile myProfile;
    private boolean isLoggedIn;

    private ClientNetworkConnection(String host) {
        gameHost = host;
        myProfile = null;
        isLoggedIn = false;
    }
    public QuestionObject[] GetQuestionsFromServer(){
        QuestionObject[] returnArray = new QuestionObject[10];
        for(int i=0; i<10;i++){
            try{
                returnArray[i] = (QuestionObject) input.readObject();
            } catch (ClassNotFoundException | OptionalDataException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        return returnArray;
    }
    public void sendScoreToServer(int Score){
        try{
            myProfile.addToOverallScore(Score);
            output.writeObject("Sending Score");
            output.flush();
            output.writeObject(Score);
            output.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Boolean didIWin(){
        String gameVictoryStatus;
        try{
            gameVictoryStatus = (String) input.readObject();
            if(gameVictoryStatus.equals("You Won")){
                myProfile.incrementNumberOfWins();
                return true;
            }
            else if(gameVictoryStatus.equals("You Lost")){
                myProfile.incrementNumberOfLosses();
                return false;
            }
            else{
                return null;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (OptionalDataException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * first string sent in the array will indicate if the user won or not, so that their data may be updated,
     *
     * then the following 2 or 4 strings will contain the username and score of the results
     */
    public String[] getGameResultsFromServer(){
        int arrayLength=0;
        String[] resultStrings;
        try{
            String gameType = (String) input.readObject();
            if(gameType.equals("Two Player Results")){
                arrayLength = 2;
            }else if(gameType.equals("Four Player Results")){
                arrayLength = 4;
            }
            else{
                arrayLength = 1;
            }
            resultStrings = new String[arrayLength];
            for(int i=0;i<arrayLength;i++){
                resultStrings[i] = (String) input.readObject();
            }
            return resultStrings;
        } catch (OptionalDataException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean connectClient() {
        try {
            serverConnection = new Socket(InetAddress.getByName(gameHost), 23495);

            input = new ObjectInputStream(serverConnection.getInputStream());
            output = new ObjectOutputStream(serverConnection.getOutputStream());
            output.flush();
            System.out.println("client success");
            return true;
        } catch (UnknownHostException e) {
            System.out.println("Host exception");
            //// TODO: 12/5/15
            return false;
        } catch (IOException e) {
            System.out.println("IOException");
            e.printStackTrace();
            return false;
        }
    }
    public String attemptToJoinGame(String gameType){
        try{
            output.writeObject(gameType);
            output.flush();
        }catch(IOException e){
            e.printStackTrace();
        }
        String serverResponse ="Error";
        boolean response = false;
        while(!response){
            try{
                Object newData = input.readObject();
                if(newData instanceof String){
                    serverResponse = (String) newData;
                    response=true;
                }
            }catch(IOException e){
                serverResponse="IOE-ERROR";
                response = true;
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                serverResponse="CNTFE-ERROR";
                response = true;
                e.printStackTrace();
            }
        }
        return serverResponse;
    }
    public String getGameStartConfirmation(){

        try {
            return (String) input.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            return "ERROR";
        }

    }
    public boolean attemptLogIn(String[] userArgs) throws IOException, ClassNotFoundException{
        try {
            for (int i = 0; i < userArgs.length; i++) {
                if(userArgs[i]!=null && output!=null) {
                    output.writeObject(userArgs[i]);
                    output.flush();
                }
            }

        } catch (IOException e) {
            throw e;
        }
        String serverResponse;
        boolean Response=false;
        while(!Response) {
            try {
                Object newData = input.readObject();
                if (newData instanceof String) {
                    serverResponse = (String) newData;
                    Response=true;
                    System.out.println(serverResponse);
                    if (serverResponse.equals("Success")) {
                        System.out.println("success");

                        myProfile = (UserProfile) input.readObject();

                        //int numWins = (Integer) input.readObject();
                        //int numLosses = (Integer) input.readObject();
                        //int totalScore = (Integer) input.readObject();
                        //myProfile = new com.UserProfile.UserProfile(userArgs[1],userArgs[2],numWins,numLosses,totalScore);
                        System.out.println(myProfile);
                        return true;
                    } else if (serverResponse.equals("Failure")) {
                        return false;
                    }
                }
            } catch (IOException e) {
                throw e;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                throw e;
            }
        }
        return false;
    }
    public void disConnect(){

        try {
            input.close();
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void closeApp(){
        ClientNetworkConnection thisInstance = getInstance();
        if(thisInstance!=null){
            thisInstance.disConnect();
        }
        thisInstance = null;
    }
    public boolean sendMessageToServer(String[] userArgs) {
        try {
            for (int i = 0; i < userArgs.length; i++) {
                output.writeObject(userArgs[i]);
                output.flush();
            }

        } catch (IOException e) {

        }
        String serverResponse;
        try {
            Object newData = input.readObject();
            if (newData instanceof String) {
                serverResponse = (String) newData;

                if (serverResponse.equals("Success")) {
                    //System.out.println("Logged in!");
                    output.writeObject("Play Game");
                } else if (serverResponse.equals("Failure")) {
                    //System.out.println("Failure");
                }
            }
        } catch (Exception e) {

        }


        return false;

    }

    public static synchronized ClientNetworkConnection getInstance() {
        if(myInstance == null){
            myInstance = new ClientNetworkConnection(hostIP);
        }
        return myInstance;
    }
}
