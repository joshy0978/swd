package com.example.joshkeeganjake.stmpedappfinal;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ToggleButton;

import java.io.IOException;

public class LoginPage extends AppCompatActivity implements View.OnClickListener {

    private EditText login_et_userName;
    private EditText login_et_password;
    private Button login_btn_login;
    private Button login_btn_register;
    private ToggleButton login_tbtn_isConnected;

    private boolean connected;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);



        login_et_userName = (EditText) findViewById(R.id.login_et_userName);
        login_et_password = (EditText) findViewById(R.id.login_et_password);
        login_btn_login = (Button) findViewById(R.id.login_btn_login);
        login_btn_register = (Button) findViewById(R.id.login_btn_register);
        login_tbtn_isConnected = (ToggleButton) findViewById(R.id.login_tbtn_isConnected);

        login_tbtn_isConnected.setClickable(false);


        login_btn_register.setOnClickListener(this);
        login_btn_login.setOnClickListener(this);
        ConnectToServer connector = new ConnectToServer();
        connector.execute();

    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.login_btn_login) {
            String[] sendUser = {"Existing User", login_et_userName.getText().toString(), login_et_password.getText().toString()};
            existingUserConnector existingUser = new existingUserConnector(sendUser);
            existingUser.execute();
        } else if (v.getId() == R.id.login_btn_register) {

            startActivity(new Intent(this, NewUser.class));

        }
    }
    public void goToLobby(){
        startActivity(new Intent(this, GameLobby.class));
    }
    public void setConnectedText(Boolean result){
        if(result){
            login_tbtn_isConnected.setText("Connected");
        }
        else{
            login_tbtn_isConnected.setText("Not Connected");
        }
    }
    private class ConnectToServer extends AsyncTask<Void,Void,Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            if(ClientNetworkConnection.getInstance().connectClient()){
                return true;
            }
            else{
                return false;
            }
        }
        @Override
        protected void onPostExecute (Boolean result){
            LoginPage.this.setConnectedText(result);
        }

    }

    private class existingUserConnector extends AsyncTask<Void,Void ,Boolean> {
        private String[] userMessage;

        public existingUserConnector(String[] userMessage){
            this.userMessage = userMessage;
        }
        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if(ClientNetworkConnection.getInstance().attemptLogIn(userMessage)){

                    return true;
                }
                else{
                    return false;
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute (Boolean result){
            if(result){
                System.out.println("Going to lobby in async");
                LoginPage.this.goToLobby();
            }
            else{
            }
        }
    }



}
