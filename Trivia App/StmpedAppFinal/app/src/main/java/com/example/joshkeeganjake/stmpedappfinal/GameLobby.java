package com.example.joshkeeganjake.stmpedappfinal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GameLobby extends AppCompatActivity {
    private Button gameLobby_btn_twoPlayer;
    private Button gameLobby_btn_fourPlayer;
    private Button gameLobby_btn_userProfile;
    private Button gameLobby_btn_quit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_lobby);

        gameLobby_btn_twoPlayer = (Button) findViewById(R.id.gameLobby_btn_twoPlayer);
        gameLobby_btn_fourPlayer = (Button) findViewById(R.id.gameLobby_btn_fourPlayer);
        gameLobby_btn_userProfile = (Button) findViewById(R.id.gameLobby_btn_userProfile);
        gameLobby_btn_quit = (Button) findViewById(R.id.gameLobby_btn_quit);






        gameLobby_btn_twoPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("two player");
                String promptServer = "Play 2 Player Game";

                StartGameConnector twoPlayerGame = new StartGameConnector(promptServer);
                twoPlayerGame.execute();
            }
        });

        gameLobby_btn_fourPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("four player");
                String promptServer = "Play 4 Player Game";
                StartGameConnector fourPlayerGame = new StartGameConnector(promptServer);
                fourPlayerGame.execute();

            }
        });

        gameLobby_btn_userProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToUserStats();
            }
        });

        gameLobby_btn_quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseApplicationAndReturnToMainActivity close = new CloseApplicationAndReturnToMainActivity();
                close.execute();
            }
        });
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        CloseApplicationAndReturnToMainActivity close = new CloseApplicationAndReturnToMainActivity();
        close.execute();
    }
    private void startGame() {
        startActivity(new Intent(this, PlayGame.class));
    }


    private void goToLogIn(){ startActivity(new Intent(this,LoginPage.class));}

    private class CloseApplicationAndReturnToMainActivity extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... params) {
            ClientNetworkConnection.getInstance().closeApp();
            return null;
        }
        protected void onPostExecute(Void param){
            GameLobby.this.goToLogIn();
        }
    }

    private class StartGameConnector extends AsyncTask<Void, Void, String> {
        private String gameIndicator;

        public StartGameConnector(String gameIndicator){
            this.gameIndicator = gameIndicator;
        }
        @Override
        protected String doInBackground(Void... params) {
            return ClientNetworkConnection.getInstance().attemptToJoinGame(gameIndicator);
        }

        @Override
        protected void onPostExecute (String serverResponse){
            if(serverResponse.equals("Success")){
                startGame();
            }else if(serverResponse.equals("Waiting For Game")){
                waitingForGame();
            }else{

            }
        }




    }

    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this).setTitle("EXIT?")
                .setMessage("Are you sure you want to Exit?")
                .setNegativeButton("No", null)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        GameLobby.super.onBackPressed();
                    }
                }).create().show();


    }

    private void waitingForGame() {
        startActivity(new Intent(this, WaitForGame.class));
    }

    private void goToUserStats() {
        startActivity(new Intent(this, UserStats.class));
    }

}
