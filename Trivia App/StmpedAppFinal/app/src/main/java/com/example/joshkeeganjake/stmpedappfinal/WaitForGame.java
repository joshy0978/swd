package com.example.joshkeeganjake.stmpedappfinal;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class WaitForGame extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wait_for_game);
        WaitingForOpponent newAsyncWaiting = new WaitingForOpponent();
        newAsyncWaiting.execute();



    }

    private void startGame() {
        startActivity(new Intent(this, PlayGame.class));
    }

    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this).setTitle("Please Wait")
                .setMessage("PLEASE WAIT!!!!!!!!!!!")
                .setNegativeButton("No", null)
                .setPositiveButton("Yes", null)
                .create()
                .show();
    }


    private class WaitingForOpponent extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            return ClientNetworkConnection.getInstance().getGameStartConfirmation();


        }

        @Override
        protected void onPostExecute(String result) {
            if(result.equals("Sending Questions")){
                WaitForGame.this.startGame();
            }
            else{

            }
        }
    }



}
