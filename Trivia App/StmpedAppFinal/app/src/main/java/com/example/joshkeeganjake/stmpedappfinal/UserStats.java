package com.example.joshkeeganjake.stmpedappfinal;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

public class UserStats extends AppCompatActivity {

    private TextView userStats_tv_UserName;
    private TextView userStats_tv_UserWins;
    private TextView userStats_tv_UserLosses;
    private TextView userStats_tv_OverallScore;
    private UserProfile userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_stats);

        userStats_tv_UserName = (TextView) findViewById(R.id.userStats_tv_userName);
        userStats_tv_UserWins = (TextView) findViewById(R.id.userStats_tv_userWins);
        userStats_tv_UserLosses = (TextView) findViewById(R.id.userStats_tv_userLosses);
        userStats_tv_OverallScore = (TextView)findViewById(R.id.userStats_tv_overallScore);
        userProfile = ClientNetworkConnection.getInstance().getMyProfile();

        userStats_tv_UserName.setText(userProfile.getUserName());
        userStats_tv_UserWins.setText("Wins: " + Integer.toString(userProfile.getNumberOfWins()));
        userStats_tv_UserLosses.setText("Losses: " + Integer.toString(userProfile.getNumberOfLosses()));
        userStats_tv_OverallScore.setText("Overall Score: " + Integer.toString(userProfile.getOverallScore()));
    }


}
