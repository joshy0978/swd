package com.example.joshkeeganjake.stmpedappfinal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Arrays;

public class GameResults extends AppCompatActivity implements View.OnClickListener {

    private ListView gameResults_lv_gameResults;
    private Button gameResults_btn_Done;
    private TextView gameResults_tv_victoryUserStatus;
    String[] playerResults;
    boolean userVictory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_results);

        gameResults_btn_Done = (Button) findViewById(R.id.gameResults_btn_Done);
        gameResults_lv_gameResults = (ListView) findViewById(R.id.gameResults_lv_gameResults);
        gameResults_tv_victoryUserStatus = (TextView) findViewById(R.id.gameResults_tv_userVictoryStatus);

        gameResults_btn_Done.setOnClickListener(this);

        GetGameResultsFromServer getResults = new GetGameResultsFromServer();
        getResults.execute();


    }

    public void displayResultsToUser(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,android.R.id.text1,playerResults);
        gameResults_lv_gameResults.setAdapter(adapter);
        if(userVictory){
            gameResults_tv_victoryUserStatus.setText("You Won!");
        }
        else{
            gameResults_tv_victoryUserStatus.setText("You Lost :(");
        }
    }

    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this).setTitle("Please Wait")
                .setMessage("Can't go back. Game is over.")
                .setNegativeButton("Return to Results", null)
                .create()
                .show();
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(this, GameLobby.class));
    }


    private class GetGameResultsFromServer extends AsyncTask<Void, Void, String[]> {



        @Override
        protected String[] doInBackground(Void... params) {
            userVictory = ClientNetworkConnection.getInstance().didIWin();
            return ClientNetworkConnection.getInstance().getGameResultsFromServer();
        }

        @Override
        protected void onPostExecute(String[] strings) {
            playerResults = strings;
            GameResults.this.displayResultsToUser();
            System.out.println(Arrays.toString(playerResults));

        }
    }

}
