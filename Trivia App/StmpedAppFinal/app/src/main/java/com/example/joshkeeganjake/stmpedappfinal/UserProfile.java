package com.example.joshkeeganjake.stmpedappfinal;

import java.io.Serializable;

/**
 * Created by joshuataylor on 12/7/15.
 */
public class UserProfile implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3869480430347025843L;
    /**
     *
     */
    private String userName;
    private String password;
    private int overallScore;
    private int numberOfWins;
    private int numberOfLosses;
    private boolean isLoggedIn;

    public UserProfile(String username, String password) {
        this(username,password,0,0,0);
    }

    public UserProfile(String username, String password, int wins, int losses,int overallScore) {
        this.userName = username;
        this.password = password;
        this.numberOfWins=wins;
        this.numberOfLosses = losses;
        this.overallScore = overallScore;
        isLoggedIn= false;
    }
    @Override
    public String toString(){
        return userName + "   \n" + password + "   \n" +"Score: " + overallScore +
                "\nWins: " + numberOfWins + "\n Losses: " + numberOfLosses;
    }

    public String getUserName() {
        return userName;
    }


    public String getPassWord() {
        return password;
    }

    public int getOverallScore() {
        return overallScore;
    }

    public void addToOverallScore(int scoreToAdd) {
        overallScore+=scoreToAdd;
    }

    public int getNumberOfWins() {
        return numberOfWins;
    }

    public void incrementNumberOfWins() {
        this.numberOfWins++;
    }

    public int getNumberOfLosses() {
        return numberOfLosses;
    }

    public void incrementNumberOfLosses() {
        this.numberOfLosses++;
    }

    public void logIn(){
        isLoggedIn=true;
    }
    public boolean isLoggedIn(){
        return isLoggedIn;
    }
    public void logOut(){
        isLoggedIn=false;
    }


}
