package com.example.joshkeeganjake.stmpedappfinal;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;

public class NewUser extends AppCompatActivity implements View.OnClickListener{

    private EditText newUser_et_username;
    private EditText newUser_et_password;
    private EditText newUser_et_reEnterPassword;
    private Button newUser_btn_registerNewUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        newUser_et_username = (EditText) findViewById(R.id.newUser_et_username);
        newUser_et_password = (EditText) findViewById(R.id.newUser_et_password);
        newUser_et_reEnterPassword = (EditText) findViewById(R.id.newUser_et_reEnterPassword);
        newUser_btn_registerNewUser = (Button) findViewById(R.id.newUser_btn_registerNewUser);

        newUser_btn_registerNewUser.setOnClickListener(this);




    }

    @Override
    public void onClick(View v) {

        String username = newUser_et_username.getText().toString();
        String pass1 = newUser_et_password.getText().toString();
        String pass2 = newUser_et_reEnterPassword.getText().toString();

        if (!username.isEmpty()) {


            if (pass1.equals(pass2) && !pass1.isEmpty() && !pass2.isEmpty()) {
                String[] sendUser = {"New User",newUser_et_username.getText().toString(),newUser_et_password.getText().toString()};
                NewUserConnector newUsr = new NewUserConnector(sendUser);
                newUsr.execute();


            } else {
                //alert user that password/username incorrec t
                System.out.println("passwords dont match or emtpy ");
            }

        } else {
            //alert saying
            System.out.println("username empty");
        }




    }

    private void goToLobby(){
        startActivity(new Intent(this, GameLobby.class));
    }

    private class NewUserConnector extends AsyncTask<Void,Void ,Boolean> {
        private String[] userMessage;

        public NewUserConnector(String[] userMessage){
            this.userMessage = userMessage;
        }
        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if(ClientNetworkConnection.getInstance().attemptLogIn(userMessage)){

                    return true;
                }
                else{
                    return false;
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute (Boolean result){
            if(result){
                System.out.println("Going to lobby in async");
                NewUser.this.goToLobby();
            }
            else{
            }
        }
    }


}
