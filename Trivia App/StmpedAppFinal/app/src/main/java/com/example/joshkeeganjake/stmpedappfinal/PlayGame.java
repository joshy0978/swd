package com.example.joshkeeganjake.stmpedappfinal;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import java.util.Arrays;
import java.util.List;

public class PlayGame extends AppCompatActivity implements View.OnClickListener{

    ImageView playGame_iv_questionImage;
    TextView playGame_tv_questionString;
    Button playGame_btn_questionNext;
    private QuestionObject[] questionBank;
    private int score;
    private int currentQuestionIndex;
    ListView playGame_lv_answerBank;
    private boolean questionsReceived;
    QuestionObject currentQuestion;
    private int userAnswerIntValue = 0;
    private int questionAnswerIntValue = 0;
    private boolean answerSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_game);

        playGame_iv_questionImage = (ImageView) findViewById(R.id.playGame_iv_imageQuestion);
        playGame_tv_questionString = (TextView) findViewById(R.id.playGame_tv_stringQuestion);
        playGame_btn_questionNext = (Button) findViewById(R.id.playGame_btn_nextQuestion);
        playGame_lv_answerBank = (ListView) findViewById(R.id.playGame_lv_answerBank);

        playGame_btn_questionNext.setOnClickListener(this);
        playGame_tv_questionString.setMovementMethod(new ScrollingMovementMethod());

        answerSelected = true;
        score = 0;
        questionBank = null;
        questionsReceived = false;
        //currentQuestionIndex=-1;

        playGame_lv_answerBank.setOnItemClickListener(
                new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        userAnswerIntValue = position;

                        for (int i = 0; i < parent.getChildCount(); i++) {
                            parent.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                        }

                        view.setBackgroundColor(Color.LTGRAY);
                        answerSelected= true;
                    }
                }

        );




        if(!questionsReceived){
            GameQuestionConnection obtainNewQuestions = new GameQuestionConnection();
            obtainNewQuestions.execute();
        }else if(currentQuestionIndex<10){
            //currentQuestion = questionBank[currentQuestionIndex];
            //playGame_tv_questionString.setText(currentQuestion.getQuestion());

        }else{
            //send score anonymous class

        }


    }
    public void setQuestionText(String questionToSet){
        playGame_tv_questionString.setText(questionToSet);
        if(currentQuestionIndex<9) {
            playGame_btn_questionNext.setText("Submit Answer: " + (currentQuestionIndex + 1));
        }
        else{
            playGame_btn_questionNext.setText("Submit Final Answer");
        }

        /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
        android.R.layout.simple_list_item_1, myStringArray);*/
        int ans_counter=0;
        for (int i = 0; i < questionBank[currentQuestionIndex].getAnswers().length-1; i++) {
            if(questionBank[currentQuestionIndex].getAnswers()[i]==null){
                ans_counter=i;
                i = questionBank[currentQuestionIndex].getAnswers().length;
            }
        }
        String[] arr = new String[ans_counter];
        for (int i = 0; i < ans_counter; i++) {
            arr[i]=questionBank[currentQuestionIndex].getAnswers()[i];
        }
        questionAnswerIntValue =Integer.parseInt(questionBank[currentQuestionIndex].getAnswers()[questionBank[currentQuestionIndex].getAnswers().length-1]);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,android.R.id.text1,arr);
        playGame_lv_answerBank.setAdapter(adapter);
    }



    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this).setTitle("No Looking Back")
                .setMessage("Can't Leave Current Game")
                .setNegativeButton("KEEP GOING!!!!", null)
                .create()
                .show();
    }

    @Override
    public void onClick(View v) {
        if(answerSelected || currentQuestionIndex==0) {
            if (questionBank != null && currentQuestionIndex < 10) {
                setQuestionText(questionBank[currentQuestionIndex].getQuestion());

            }
            if (userAnswerIntValue == questionAnswerIntValue) {
                score += 1;
                currentQuestionIndex++;
                userAnswerIntValue = -1;
            } else {
                currentQuestionIndex++;
            }
            if (currentQuestionIndex == 10) {
                SendScoreToServer sendIt = new SendScoreToServer();
                sendIt.execute();
                startActivity(new Intent(this, GameResults.class));
            }
            System.out.println("User Score: " + score);
            answerSelected=false;
        }

    }
    private class SendScoreToServer extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... params) {
            ClientNetworkConnection.getInstance().sendScoreToServer(score);
            System.out.println("Sent Score");
            return null;
        }
    }
    private class GameQuestionConnection extends AsyncTask <Void, Void, QuestionObject[]> {

        @Override
        protected QuestionObject[] doInBackground(Void... params) {
            return ClientNetworkConnection.getInstance().GetQuestionsFromServer();
        }

        @Override
        protected void onPostExecute(QuestionObject[] questionObjects) {
            questionBank = questionObjects;
            questionsReceived = true;
            currentQuestionIndex=0;

        }
    }



}
