import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.jsoup.Jsoup;

/**
 * The QuestionParser class takes in a string for the xml file name for the constructor.
 * This class has methods that retrieves certain text content. Each content has html formatiing
 * in which then the jsoup package created by "Jonathan Hedly" under "The MIT License" extracts 
 * just the text string. Then the parseQuestionObjects method builds each question object and adds it
 * to the array list.
 * 
 * The question.xml file is from http://web-cat.org/questionbank/
 * 
 * @author Jacob Veal
 *
 */
public class QuestionParser {

	Document doc;
	NodeList nl;

	public QuestionParser(String filename) {
		
		parseXmlFile(filename);
		parseQuestionObjects();

	}

	private void parseXmlFile(String filename) {
		
		// get the factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {
			// Using factory get an instance of document builder
			DocumentBuilder db = dbf.newDocumentBuilder();
			
			// parse using builder to get DOM representation of the XML file
			doc = db.parse(filename);
			Element docEle = doc.getDocumentElement();
			
			//each object is separated by a item tag
			nl = docEle.getElementsByTagName("item");
			
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (SAXException se) {
			se.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public ArrayList<QuestionObject> parseQuestionObjects() {
	
		ArrayList<QuestionObject> questionArray = new ArrayList<QuestionObject>();

		if (nl != null && nl.getLength() > 0) {
			for (int i = 0; i < nl.getLength(); i++) {

				// get the question element
				Element el = (Element) nl.item(i);

				// get the object
				getImage(el, "matimage");
				QuestionObject obj = new QuestionObject(getQuestion(el, "material"), getAnswers(el, "response_label"),
						getQuestion(el, "itemfeedback"));
				
				// add it to list
				questionArray.add(obj);

			}
		}
		return questionArray;
	}

	private String getQuestion(Element ele, String tagName) {
		String textVal = null;
		NodeList nl = ele.getElementsByTagName(tagName);

		if (nl != null && nl.getLength() > 0) {
			Element el = (Element) nl.item(0);
			textVal = el.getTextContent();
			StringBuilder sb = new StringBuilder();
			sb.append(textVal);

			try {
				return extractText(sb);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return null;

	}

	/**
	 * Retrieves the questions answers and places them into an array.
	 * The correct answer's index is placed in the arrays last index.
	 * 
	 * @param elem
	 * @param tagName
	 * @return
	 */
	private String[] getAnswers(Element elem, String tagName) {

		NodeList nl = elem.getElementsByTagName(tagName);
		NodeList nl2 = elem.getElementsByTagName("setvar");

		String[] array = new String[nl.getLength() + 1];

		if (nl != null && nl.getLength() > 0) {
			for (int i = 0; i < nl.getLength(); i++) {
				Element el = (Element) nl.item(i);
				Element el2 = (Element) nl2.item(i);

				StringBuilder sb = new StringBuilder();
				sb.append(el.getTextContent());

				try {
					String line = extractText(sb);
					if (!line.equals("\"")) {
						array[i] = line;
						if (el2.getTextContent().equals("1")) {
							array[nl.getLength()] = String.valueOf(i);

						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

		}
		return array;

	}

	/**
	 * Returns the images id in the xml file
	 * 
	 * @param el
	 * @param tagName
	 * @return
	 */
	private String getImage(Element el, String tagName) {

		NodeList nl2 = el.getElementsByTagName(tagName);

		if (nl2 != null && nl2.getLength() > 0) {
			Element elem = (Element) nl2.item(0);

			if (elem.hasAttribute("uri")) {
				System.out.println(elem.getAttribute("uri"));

			}
		}
		return null;
	}

	private String extractText(StringBuilder sb) throws IOException {

		String textOnly = Jsoup.parse(sb.toString()).text();
		return textOnly;
	}

	public static void main(String[] args) {

		QuestionParser qp = new QuestionParser("questions.xml");
	}

}
