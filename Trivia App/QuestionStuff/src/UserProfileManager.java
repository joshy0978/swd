import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * 
 * The UserProfileManager class has three methods that read and write to the
 * user profile xml file. AddUser adds a user to the end of the xml file. Get
 * user returns the specified users information in a string array. The
 * updateUser accesses that user in the xml file and writes to it.
 * 
 * @author Jacob Veal
 *
 */
public class UserProfileManager {

	Document doc;
	NodeList nl;
	String fileName;
	Element root;

	public UserProfileManager(String filename) {

		this.fileName = filename;

		try {
			// File userFile = new File(filename);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			// dbf.setIgnoringElementContentWhitespace(true);
			// dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			doc = db.parse(fileName);

			nl = doc.getElementsByTagName("user");

			System.out.println("root of xml file" + doc.getDocumentElement().getNodeName());
			// doc = db.parse(fileName);

		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void addUser(String username, String password) {

		Element newUser = doc.createElement("user");
		newUser.setAttribute("userID", String.valueOf(nl.getLength()));

		Element userName = doc.createElement("username");
		userName.appendChild(doc.createTextNode(username));
		newUser.appendChild(userName);

		Element passWord = doc.createElement("password");
		passWord.appendChild(doc.createTextNode(password));
		newUser.appendChild(passWord);

		Element wins = doc.createElement("wins");
		wins.appendChild(doc.createTextNode("0"));
		newUser.appendChild(wins);

		Element loses = doc.createElement("losses");
		loses.appendChild(doc.createTextNode("0"));
		newUser.appendChild(loses);

		Element root = doc.getDocumentElement();
		root.appendChild(newUser);

		try {

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(fileName);
			transformer.transform(source, result);

		} catch (TransformerException e) {

			e.printStackTrace();
		}

	}

	public UserProfile getUser(String idNum) {

		String[] userArray = new String[4];
		int arrayCounter = 0;

		for (int i = 0; i < nl.getLength(); i++) {
			Element user = (Element) nl.item(i);

			if (user.getAttribute("userID").equals(idNum)) {
				NodeList nodeList = user.getChildNodes();
				for (int j = 0; j < nodeList.getLength(); j++) {
					if (nodeList.item(j) instanceof Element) {
						userArray[arrayCounter++] = nodeList.item(j).getTextContent().trim();
					}
				}
			}

		}
		return new UserProfile(userArray[0], userArray[1], Integer.parseInt(userArray[2]),
				Integer.parseInt(userArray[3]), 0);

	}

	public void updateUser(UserProfile userProf) {
		
		for (int i = 0; i < nl.getLength(); i++) {
			Node node = nl.item(i);

			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element el = (Element) node;
				NodeList nodes = el.getElementsByTagName("username");
				Node nod = (Node) nodes.item(0);

				if (nod.getFirstChild().getNodeValue().equals(userProf.getUserName())) {

					nodes = el.getElementsByTagName("wins");
					nod = (Node) nodes.item(0);
					nod.getFirstChild().setTextContent(String.valueOf(userProf.getNumberOfWins()));

					nodes = el.getElementsByTagName("losses");
					nod = (Node) nodes.item(0);
					nod.getFirstChild().setNodeValue(String.valueOf(userProf.getNumberOfLosses()));
				}

			}

			try {
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(fileName);
				transformer.transform(source, result);
			} catch (TransformerException e) {

				e.printStackTrace();
			}

		}
	}
}
