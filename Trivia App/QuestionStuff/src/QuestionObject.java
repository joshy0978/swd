import java.awt.Image;

/**
 * Question Object will hold all the information about each question and provides methods to 
 * retrieve the information.
 * 
 * Created by Jacob on 12/4/15.
 */
public class QuestionObject {

	private String question;
	private String explanation;
	private String[] answers;
	private Image image;

	public QuestionObject(String question, String[] answers) {
		this.question = question;
		this.answers = answers;
		this.explanation = null;
		this.explanation = null;
	}

	public QuestionObject(String question, String[] answers, String explanation) {
		this.question = question;
		this.answers = answers;
		this.explanation = explanation;
		this.image = null;
	}

	public QuestionObject(String question, String[] answers, String explanation, Image image) {
		this.question = question;
		this.answers = answers;
		this.explanation = explanation;
		this.image = image;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public String[] getAnswers() {
		return answers;
	}

	public void setAnswers(String[] answers) {
		this.answers = answers;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}
	
	public void printAnswers(){
		for(String line: answers){
			if(line != null){
				System.out.println(line);
			}
		}
	}

}