
public class UPMDriver {
	public static void main(String[] args) {
		UserProfileManager upm = new UserProfileManager("UserProfile.xml");
		// UserProfile up = new UserProfile("keegan","gohawks");
		// upm.addUser("keegan", "gohawks");
	    UserProfile up1 = new UserProfile("Jacob1", "gohawks");
		UserProfile newUsr2 = new UserProfile("keegan2", "gohawks");
		UserProfile newUsr3 = new UserProfile("keegan3", "gohawks");
		UserProfile newUsr4 = new UserProfile("keegan4", "gohawks");

		upm.addUser("Jacob1", "gohawks");
		UserProfile up2 = upm.getUser("3");
		System.out.println(up2.getUserName());
		System.out.println(up2.getPassWord());
		System.out.println(up2.getNumberOfWins());
		System.out.println(up2.getNumberOfLosses());
		System.out.println(up2.getOverallScore());
		// upm.addUser("keegan2", "gohawks");
		// upm.addUser("keegan3", "gohawks");
		// upm.addUser("keegan4", "gohawks");

		// UserProfile up1 = upm.getUser("1");
		up1.incrementNumberOfLosses();
		up1.incrementNumberOfWins();
		upm.updateUser(up1);
		UserProfile up3 = upm.getUser("3");
		System.out.println(up3.getUserName());
		System.out.println(up3.getPassWord());
		System.out.println(up3.getNumberOfWins());
		System.out.println(up3.getNumberOfLosses());
		System.out.println(up3.getOverallScore());
		
		up1.incrementNumberOfLosses();
		up1.incrementNumberOfWins();
		upm.updateUser(up1);
		UserProfile up4 = upm.getUser("3");
		System.out.println(up4.getUserName());
		System.out.println(up4.getPassWord());
		System.out.println(up4.getNumberOfWins());
		System.out.println(up4.getNumberOfLosses());
		System.out.println(up4.getOverallScore());

	}

}
