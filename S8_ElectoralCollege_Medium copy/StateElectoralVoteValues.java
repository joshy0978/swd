

/**
 * Enumeration class StateElectoralVoteValues that holds the 
 * State name and their respective Electoral Vote Value
 */
public enum StateElectoralVoteValues {

    ALABAMA ("Alabama",9),
    ALASKA ("Alaska",3),
    ARIZONA ("Arizona",11),
    ARKANSAS ("Arkansas",6),
    CALIFORNIA ("California",55),
    COLORADO ("Colorado",9),
    CONNECTICUT ("Connecticut",7),
    DELAWARE ("Delaware",3),
    FLORIDA ("Florida",29),
    GEORGIA ("Georgia",16),
    HAWAII ("Hawaii",4),
    IDAHO ("Idaho",4),
    ILLINOIS ("Illinois",20),
    INDIANA ("Indiana",11),
    IOWA ("Iowa",6),
    KANSAS("Kansas",6),
    KENTUCKY("Kentucky",8),
    LOUISIANA("Louisiana",8),
    MAINE1("Maine 1st",1),
    MAINE2("Maine 2nd",1),
    MAINEP("Maine Popular",2),
    MARYLAND("Maryland",10),
    MASSACHUSETTS("Massachusetts",11),
    MICHIGAN("Michigan",16),
    MINNESOTA("Minnesota",10),
    MISSISSIPPI("Mississippi",6),
    MISSOURI("Missouri",10),
    MONTANA("Montana",3),
    NEBRASKA1ST("Nebraska 1st",1),
    NEBRASKA2ND("Nebraska 2nd",1),
    NEBRASKA3RD("Nebraska Popular",2),
    NEVADA("Nevada",6),
    NEWHAMPSHIRE("New Hampshire",4),
    NEWJERSEY("New Jersey",14),
    NEWMEXICO("New Mexico",5),
    NEWYORK("New York",29),
    NORTHCAROLINA("North Carolina",15),
    OHIO("Ohio",18),
    OKLAHOMA("Oklahoma",7),
    OREGON("Oregon",7),
    PENNSYLVANIA("Pennsylvania",20),
    RHODEISLAND("Rhode Island",4),
    SOUTHCAROLINA("South Carolina",9),
    SOUTHDAKOTA("South Dakota",3),
    TENNESSE("Tennesse",11),
    TEXAS("Texas",38),
    UTAH("Utah",6),
    VERMONT("Vermont",3),
    VIRGINIA("Virginia",13),
    WASHINGTON("Washington",12),
    WESTVIRGINIA("West Virginia",5),
    WISCONSIN("Wisconsin",10),
    WASHINGTONDC("Washington, D.C.",3);

    private final String stateName;
    private final int valueOfElectoralVotes;
    
    /**
     * StateElectoralVoteValues constructor that sets the instance variable stateName
     * and valueOfElectralVotes.
     * @param stateName string of State name
     * @param valueOfElectoralVotes int value of electoral value
     */
    StateElectoralVoteValues(String stateName, int valueOfElectoralVotes) {
        this.stateName = stateName;
        this.valueOfElectoralVotes = valueOfElectoralVotes;
    }
    
    /**
     * getStateName getter for instance variable stateName
     * @return string of instance variable stateName
     */
    public String getStateName() {
        return stateName;
    }
    
    /**
     * getValueOfElectoralVotes getter for instance variable valueOfElectralVotes
     * @return int of instance variable valueOfElectralVotes
     */
    public int getValueOfElectoralVotes() {
        return valueOfElectoralVotes;
    }
}
