

/**
 * Created by joshuataylor on 10/11/15.
 */
public enum PartyAffiliation {

    DEMOCRATIC("Democratic"),
    REPUBLICAN("Republican"),
    UNDECIDED("Undecided");
	
    private final String partyName;

    /**
     * PartyAffiliation is an setter method for the partyName variable.
     * @param partyName is a string
     */
    PartyAffiliation(String partyName) {
        this.partyName = partyName;
    }
    
    /**
     * getPartyName is getter method
     * @return string of partyName
     */
    public String getPartyName() {
        return partyName;
    }


}
