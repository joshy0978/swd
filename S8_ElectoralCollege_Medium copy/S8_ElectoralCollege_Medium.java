

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.util.ArrayList;

/**
 * Created by joshuataylor on 9/20/15.
 */
public class S8_ElectoralCollege_Medium implements ActionListener {

    private JFrame frame;
    private JLabel demWinnerDisplayLabel;
    private JLabel repWinnerDisplayLabel;
    private JLabel undWinnerDisplayLabel;
    private JLabel statusWinnerDisplayLabel;
    private ArrayList<PartyRadioButton> buttons;



    /**
     * Constructor for class S8_ElectoralCollege_Medium.  Takes in no parameters and returns void.  
     * Sets up Frame that holds JPanels that make up each state JLabel and the JRadio Buttons that 
     * contain each choice to vote for.  
     */
    S8_ElectoralCollege_Medium () {

        //format frame
        frame = new JFrame("Electoral College");
        frame.setSize(2500, 2500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout());
        buttons = new ArrayList<PartyRadioButton>();

        JPanel labelContainer = new JPanel();
        demWinnerDisplayLabel = new JLabel("Democratic Party Total: 0");
        repWinnerDisplayLabel = new JLabel("Republican Party Total: 0");
        undWinnerDisplayLabel = new JLabel("Undecided Total: 0");
        statusWinnerDisplayLabel = new JLabel("Status: Undecided");

        labelContainer.add(demWinnerDisplayLabel);
        labelContainer.add(repWinnerDisplayLabel);
        labelContainer.add(undWinnerDisplayLabel);
        labelContainer.add(statusWinnerDisplayLabel);
        frame.add(labelContainer);

        JPanel buttonPanel = new JPanel(new GridLayout(20, 10, 20, 5));
        for (StateElectoralVoteValues state: StateElectoralVoteValues.values()) {

            String stateName = state.getStateName();
            int electoralStateValue = state.getValueOfElectoralVotes();

            String previousButtonTitle;


            JLabel stateNameLabel = new JLabel(stateName);
            //stateNameLabel.setVerticalAlignment(SwingConstants.LEADING);

            //Setup each button
            PartyRadioButton democraticChoice = new PartyRadioButton(PartyAffiliation.DEMOCRATIC, state.getValueOfElectoralVotes());
            PartyRadioButton republicanChoice = new PartyRadioButton(PartyAffiliation.REPUBLICAN, state.getValueOfElectoralVotes());
            PartyRadioButton undecidedChoice = new PartyRadioButton(PartyAffiliation.UNDECIDED, state.getValueOfElectoralVotes());

            //Group Buttons together so only one can be selected at a time
            ButtonGroup politicalChoiceButtons = new ButtonGroup();
            politicalChoiceButtons.add(democraticChoice);
            politicalChoiceButtons.add(republicanChoice);
            politicalChoiceButtons.add(undecidedChoice);

            undecidedChoice.setSelected(true);

            undecidedChoice.addActionListener(this);
            republicanChoice.addActionListener(this);
            democraticChoice.addActionListener(this);
            buttons.add(undecidedChoice);
            buttons.add(republicanChoice);
            buttons.add(democraticChoice);

            //trying jpanel
            JPanel stateContainer = new JPanel();
            stateContainer.add(stateNameLabel);
            stateContainer.add(democraticChoice);
            stateContainer.add(republicanChoice);
            stateContainer.add(undecidedChoice);

            //StatePanel panel = new StatePanel(StateElectoralVoteValues)

            buttonPanel.add(stateContainer);

        }

        frame.add(buttonPanel);

        frame.setVisible(true);

    }

    /**
     * actionPerformed is called everytime A JRadioButton is clicked.  
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Selected Button: ");
        int repTotal = 0;
        int demTotal = 0;
        int undecidedTotal = 0;

        for(PartyRadioButton b : buttons) {
            if (b.isSelected()) {
                if( b.getParty() == PartyAffiliation.DEMOCRATIC ) {
                    demTotal += b.getElectoralVoteCount();
                } else if (b.getParty() == PartyAffiliation.REPUBLICAN) {
                    repTotal += b.getElectoralVoteCount();
                } else if (b.getParty() == PartyAffiliation.UNDECIDED) {
                    undecidedTotal += b.getElectoralVoteCount();
                }
            }

        }
        System.out.println(repTotal);
        System.out.println(demTotal);
        System.out.println();
        demWinnerDisplayLabel.setText("Democratic Party Total: " + demTotal);
        repWinnerDisplayLabel.setText("Republican Party Total: " + repTotal);
        undWinnerDisplayLabel.setText("Undecided Total: " + undecidedTotal);
        if (demTotal >= 270) {
            statusWinnerDisplayLabel.setText("Status: Democrats Win!");
        } else if (repTotal >= 270) {
            statusWinnerDisplayLabel.setText("Status: Republicans Win!");
        } else {
            statusWinnerDisplayLabel.setText("Status: Undecided");
        }
    }
}
