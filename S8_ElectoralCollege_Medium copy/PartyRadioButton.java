
import javax.swing.*;

/**
 * PartyRadioButton is a subclass of JRadioButton
 */
public class PartyRadioButton extends JRadioButton {

    private PartyAffiliation party;
    private int electoralVoteCount;

    /**
     * PartyRadioButton constructor that sets instance variable
     * party and electoralVoteCount
     * @param party PartyAffiliation enumeration value
     * @param electoralVoteCount int value 
     */
    public PartyRadioButton(PartyAffiliation party, int electoralVoteCount) {
        this.party = party;
        this.electoralVoteCount = electoralVoteCount;
        this.setText(party.getPartyName());
    }

    /**
     * PartyAffiliation getter method that returns of type enum class
     * @return PartyAffiliation enum
     */
    public PartyAffiliation getParty() {
        return party;
    }

    /**
     * getElectoralVoteCount getter method that returns 
     * instance variable electoralVoteCount
     * @return int value of instance variable electoralVoteCount
     */
    public int getElectoralVoteCount() {
        return electoralVoteCount;
    }
}
