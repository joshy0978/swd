import java.awt.BorderLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BoxLayout;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


/**
 * Class ImageRotatorJFrame
 * @author joshuataylor
 *
 */
public class ImageRotatorJFrame extends JFrame{
	
	
	/**
	 * instance variable of type ImageJPane
	 */
	private ImageJPanel imageToRotate;
	
	/**
	 * instance variable of type JTextField
	 */
	private JTextField rotateDegreeLabel;
	
	/**
	 * instance variable of type JCheckBox
	 */
	private JCheckBox spinContinouslyCheckBox;
	
	/**
	 * instance variable of type JSlider
	 */
	private JSlider dynamicallyChangeSpinSlider;

	
	/**
	 * Constructor that instantiate an object of Type ImageRotatorJFrame.
	 * 
	 */
	ImageRotatorJFrame() {
		
		//JFrame super Class constructor
		super("Image Rotator");
		
		//this.setLayout(new BoxLayout(getContentPane(),BoxLayout.Y_AXIS));
		
		
		//instantiate new variables
		imageToRotate = new ImageJPanel();
		rotateDegreeLabel = new JTextField("Rotate Image How Many Degrees");
		spinContinouslyCheckBox = new JCheckBox("Spin Continously");
		dynamicallyChangeSpinSlider = new JSlider(JSlider.HORIZONTAL, 1, 10, 1);
		

		
		//setup Slider
		dynamicallyChangeSpinSlider.setMinorTickSpacing(1);
		dynamicallyChangeSpinSlider.setPaintTicks(true);
		dynamicallyChangeSpinSlider.setPaintLabels(true);
		dynamicallyChangeSpinSlider.setSnapToTicks(true);
		dynamicallyChangeSpinSlider.setLabelTable(dynamicallyChangeSpinSlider.createStandardLabels(1));
		
		//JPanel to hold image
		JPanel imageContainer = new JPanel();
		BoxLayout imageLayout = new BoxLayout(imageContainer, BoxLayout.Y_AXIS);
		imageContainer.setLayout(imageLayout);
		imageContainer.add(imageToRotate);
		

		//JPanel to hold controllers
		JPanel controlPanel = new JPanel();
		BoxLayout layout = new BoxLayout(controlPanel, BoxLayout.Y_AXIS);
		controlPanel.setLayout(layout);
		
		//Add Controls to controlPanel
		controlPanel.add(rotateDegreeLabel);
		controlPanel.add(spinContinouslyCheckBox);
		controlPanel.add(dynamicallyChangeSpinSlider);
	
		
		
		this.getContentPane().add(BorderLayout.SOUTH,controlPanel);
		
		this.getContentPane().add(BorderLayout.CENTER,imageContainer);
		
		
		rotateDegreeLabel.addActionListener(new TextBoxHelper());
		spinContinouslyCheckBox.addItemListener(new CheckBoxHelper());
		dynamicallyChangeSpinSlider.addChangeListener(new SliderHelper());
		
		super.setSize(450,350);
		super.setDefaultCloseOperation(EXIT_ON_CLOSE);
		super.setVisible(true);
		
		
	}
	
	/**
	 * TextBoxHelper is a inner class that implements ActionListener.  This class is the 
	 * actionListener for the JTextBox in the outer class. 
	 * @author joshuataylor
	 *
	 */
	private class TextBoxHelper implements ActionListener {
		

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub

			String textBoxInput = rotateDegreeLabel.getText();
			
			int degreeToRotateFromString = Integer.parseInt(textBoxInput);
			
			//ImageRotatorJFrame.this.imageToRotate.setRadiansToRotate(degreeToRotateFromString);
			imageToRotate.setRadiansToRotate(degreeToRotateFromString);
			
			System.out.println(textBoxInput);
	}
		
	}
	
	/**
	 * CheckBoxHelper is a inner class that implements ItemListener.  This class is the 
	 * actionListener for the JCheckBox in the outer class. 
	 * @author joshuataylor
	 *
	 */
	private class CheckBoxHelper implements ItemListener {

		@Override
		public void itemStateChanged(ItemEvent e) {
			// TODO Auto-generated method stub
			
			System.out.println(e.getStateChange());
			
			if (e.getStateChange() == ItemEvent.SELECTED) {
				int startInt = ImageRotatorJFrame.this.dynamicallyChangeSpinSlider.getValue() * 100;
				//ImageRotatorJFrame.this.imageToRotate.startTimer(startInt);
				imageToRotate.startTimer(startInt);
			} else {
				ImageRotatorJFrame.this.imageToRotate.endTimer();
			}
			
		}
		
	}
	
	/**
	 * SliderHelper is a inner class that implements ChangeListerner.  This class is the 
	 * actionListener for the JSlider in the outer class. 
	 * @author joshuataylor
	 *
	 */
	private class SliderHelper implements ChangeListener {

		
		@Override
		public void stateChanged(ChangeEvent e) {
			
			JSlider sliderValue = (JSlider) e.getSource();
			
			int value = (100 * sliderValue.getValue());
			
			if(ImageRotatorJFrame.this.spinContinouslyCheckBox.isSelected())
				ImageRotatorJFrame.this.imageToRotate.changeDelay(value);
				
				
		
			
		}
		
	}
		
}
	






