import java.awt.Graphics;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.*;


/**
 * Class ImageJpanel extends JPanel
 * @author joshuataylor
 *
 */
public class ImageJPanel extends JPanel {
	
	
	/**
	 * instance variable of type Image
	 */
	private Image image;
	/**
	 * instance variable of type int x
	 */
	private int x = this.getWidth()/2;
	/**
	 * instance variable of type int y
	 */
	private int y = this.getHeight()/2;
	/**
	 * instance variable of type int that holds the radiansToRotate value
	 */
	private int radiansToRotate = 0;
	/**
	 * instance variable of type timer
	 */
	private Timer timer;
	/**
	 * instance variable of type int 
	 */
	private int continousTurnVariable;
	
	
	/**
	 * Constructor that instantiates an the instance variables timer and image. 
	 */
	ImageJPanel() {
		
		timer = new Timer(100, actionListner);
		image = new ImageIcon("40x40px-ZC-22b9f69e_AluminumAppleLogo.png").getImage();
	}
	

	/**
	 * paintComponent method responsible for drawing the image on screen. 
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		x = this.getWidth()/2;
		y = this.getHeight()/2;
		
		AffineTransform transform = AffineTransform.getTranslateInstance(x,y);
		Graphics2D g2d = (Graphics2D)g;
			
		transform.rotate(Math.toRadians(radiansToRotate));
		
		g2d.drawImage(image,transform, null);
		
		
	}
	

	/**
	 * setRadiansToRotate is a method that sets the instance variable radiansToRotate.
	 * @param radiansToRotate 
	 */
	public void setRadiansToRotate(int radiansToRotate) {
		this.radiansToRotate = radiansToRotate;
		
		repaint();
	}
	
	/**
	 * startTimer is a method that sets the timer Delay and starts  the timer
	 * @param timerSpeed
	 */
	public void startTimer(int timerSpeed) {
	
		timer.setDelay(timerSpeed);
		
		timer.start();
			
		
	}
	
	/**
	 * endTimer stops the currently running timer
	 */
	public void endTimer() {
		
		timer.stop();
		
	}
	
	/**
	 * changeDelay is a setter method that sets the timers delay
	 * @param time
	 */
	public void changeDelay(int time) {
		timer.setDelay(time);
	}
	
	/**
	 * action to perform each time the timer has counted to zero
	 */
	ActionListener  actionListner = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent event) {
			
			int rotate = ImageJPanel.this.radiansToRotate + 10;
			
			ImageJPanel.this.setRadiansToRotate(rotate);
			
		}
	};
	
	

	
}
