import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * RandomCircle_Easy extends JPanel. The class has 7 instance variables.  3 of them are JTextAreas that
 * are used to is display the diameter, area, and circumference. The other 4 variable are of type final
 * int and named radius, diameter, area, circumference.
 */
public class RandomCircle_Easy extends JPanel{


    private JTextArea diameterTextArea;
    private JTextArea areaTextArea;
    private JTextArea circumferenceTextArea;

    private final int radius = (int) (Math.random() * 200 + 50);
    private final int diameter = 2 * radius;
    private final int area = (int)  (Math.PI * radius * radius);
    private final int circumference = (int) (2 * Math.PI * radius);

    /**
     * paintComponent
     * @param g panel is a Graphics object that is passed from the GUI Panel.
     */

    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        g.setColor(Color.blue);
        int width = getWidth();
        int height = getHeight();

        g.fillOval(width / 2 - radius, height / 2 - radius, diameter, diameter);
    }

    /**
     *
     * Default Class Constructor.  The constructor takes in no parameters.
     * Within the constructor 3 JTextArea are created for holding the calculated diameter, area, and circumference;
     * The 3 JTextARea are then added to class itself.  The class itself is an extension of JPanel
     */

    public RandomCircle_Easy() {

        diameterTextArea = new JTextArea("Diameter: " + getDiameter());
        diameterTextArea.setSize(10, 10);
        diameterTextArea.setEditable(false);
        diameterTextArea.setVisible(true);

        areaTextArea = new JTextArea("Area: " + getArea());
        areaTextArea.setSize(10, 10);
        areaTextArea.setEditable(false);
        areaTextArea.setVisible(true);

        circumferenceTextArea = new JTextArea("Circumference: " + getCircumference());
        circumferenceTextArea.setSize(10, 10);
        circumferenceTextArea.setEditable(false);
        circumferenceTextArea.setVisible(true);

        JPanel textAreaContainer = new JPanel();
        textAreaContainer.add(diameterTextArea);
        textAreaContainer.add(areaTextArea);
        textAreaContainer.add(circumferenceTextArea);

        textAreaContainer.setVisible(true);

        this.add(textAreaContainer);


    }

    /**
     *getRadius returns type int.
     * @return the value of radius that is generated when the class is created.
     */

    public int getRadius() {
        return radius;
    }

    /**
     * getDiameter returns type String.
     * @return value of diameter
     */
    public String getDiameter() {
        return String.valueOf(diameter);
    }

    /**
     * getArea returns type String.
     * @return value of area
     */

    public String getArea() {
        return String.valueOf(area);
    }

    /**
     * getCircumference returns type String.
     * @return value of circumference
     */
    public String getCircumference() {
        return String.valueOf(circumference);
    }
}
