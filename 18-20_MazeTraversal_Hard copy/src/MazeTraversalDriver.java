/**
 * MazeTraversalDriver is test class for MazeTraversal.  MazeTraversalDriver
 * creates a maze that is passed to a MazeTraversal object.
 * @author joshuataylor
 *
 */
public class MazeTraversalDriver {

	
    public static void main(String[] args) {
    	/**
    	 * mazeArray is a 2d array of type char[][]
    	 * that holds the array that will be tested
    	 */
        char[][] mazeArray = {  {'#','#','#','#','#','#','#','#','#','#','#','#'},
                                {'#','.','.','.','#','.','.','.','.','.','.','#'},
                                {'.','.','#','.','#','.','#','#','#','#','.','#'},
                                {'#','#','#','.','#','.','.','.','.','#','.','#'},
                                {'#','.','.','.','.','#','#','#','.','#','.','G'},
                                {'#','#','#','#','.','#','.','#','.','#','.','#'},
                                {'#','.','.','#','.','#','.','#','.','#','.','#'},
                                {'#','#','.','#','.','#','.','#','.','#','.','#'},
                                {'#','#','.','#','.','#','.','#','.','#','.','#'},
                                {'#','.','.','.','.','.','.','.','.','#','.','#'},
                                {'#','#','#','#','#','#','.','#','#','#','.','#'},
                                {'#','.','.','.','.','.','.','#','.','.','.','#'},
                                {'#','#','#','#','#','#','#','#','#','#','#','#'}};

        
        //getting familiar with how a 2d array works
//        System.out.println(mazeArray.length);
        //System.out.println(mazeArray[2][0]);

        /**
         * testMaze is of type MazeTraversal. the constructor
         * passes in type char[][] that will be the maze we 
         * want to traverse
         */
        MazeTraversal testMaze = new MazeTraversal(mazeArray);

        testMaze.mazeTraversal(2,0);

    }




}