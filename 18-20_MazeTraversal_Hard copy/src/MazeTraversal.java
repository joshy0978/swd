/**
 * MazeTraversal is a class that recursively solves a maze
 * 
 * @author joshuataylor
 *
 */
public class MazeTraversal {

	/**
	 * maze is type char[][] that is an instance variable that holds the maze
	 * the class with be testing.
	 */
	private char[][] maze;

	/**
	 * robotMove is an instance variable that holds the char 'x' to help with
	 * readability in code
	 */
	private final char robotMove = 'x';

	/**
	 * robotBackTrack is an instance variable that holds the char '+' to heop
	 * with readability in code.
	 */
	private final char robotBackTrack = '+';

	private final char wall = '#';

	/**
	 * Constructor that sets the instance variable maze with
	 * 
	 * @param maze
	 *            is type char[][] that is the maze the class will solve
	 */
	MazeTraversal(char[][] maze) {

		this.maze = maze;
	}

	/**
	 * mazeTraversal takes in a position in the maze and test weather it is a
	 * valid move.
	 * 
	 * @param row
	 *            value of position to test
	 * @param column
	 *            value of position to test
	 * @return true or false
	 */
	public boolean mazeTraversal(int row, int column) {

		// 1st
		// check to make sure you're within the maze
		// must check row first then column
		// checking row
		if (row < 0 || row > maze.length - 1) {
			return false;
		}
		// checking column
		if (column < 0 || column > maze[row].length - 1) {

			return false;
		}

		// 2nd
		// check if found exit
		if (maze[row][column] == 'G') {
			return true;
		}

		// 3rd
		// check for wall or previous path( dont want to back track)
		if (maze[row][column] == wall || maze[row][column] == robotMove) {
			return false;
		}

		// 4th
		// mark spot on the maze
		// with robotMove('x')
		maze[row][column] = robotMove;
		printMaze();
		System.out.println("");

		// 5th check surrounding spots to see if free or not
		// Order of Check: North, East, South, West

		// check North of position to see if it is open
		// If open return true

		if (mazeTraversal((row - 1), column) == true) {
			return true;
		}

		// check East of position to see if it is open
		// If open return true
		if (mazeTraversal(row, (column + 1)) == true) {
			return true;
		}

		// check South of position to see if it is open
		// If open return true
		if (mazeTraversal(row + 1, column) == true) {
			return true;
		}

		// check West of position to see if it is open
		// If open return true
		if (mazeTraversal(row, column - 1) == true) {
			return true;
		}

		//Reach this if you gone the wrong way
		
		// If you've backed yourself in a corner retreat
		// it's not the path you are looking for
		// and leave a reminder so you know youve been there
		maze[row][column] = robotBackTrack;
		printMaze();
		System.out.println("");

		// Since you cant do anyting return false
		return false;

	}
	/**
	 * convenience  method that prints out the instance variable maze
	 */
	public void printMaze() {
		for (int i = 0; i < maze.length; i++) {
			for (int j = 0; j < maze[i].length; j++) {
				System.out.print(maze[i][j] + " ");
			}
			System.out.println("");
		}
	}
}
