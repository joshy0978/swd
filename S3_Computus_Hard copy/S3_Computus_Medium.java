

import java.util.*;

/**
 * Created by joshuataylor on 9/10/15.
 */
public class S3_Computus_Medium {

    static private int year;

    /**
     *
     * @return class static variable int year
     */
    public static int getYear() {
        return year;
    }

    /**
     * Sets the static variable year
     * @param year is type int
     */
    public static void setYear(int year) {
        S3_Computus_Medium.year = year;
    }

    /**
     *
     * @param year is type int
     * @return type String and is Month and Date of
     */
    public static String calculateEasterDate (int year) {
        setYear(year);
        int a = year % 19;
        int b = year / 100;
        int c = year % 100;
        int d = b / 4;
        int e = b % 4;
        int f = (b + 8) / 25;
        int g = (b - f + 1) / 3;
        int h = (19 * a + b - d - g + 15) % 30;
        int i = c / 4;
        int k = c % 4;
        int l = (32 + 2 * e + 2 * i - h - k) % 7;
        int m = (a + 11 * h + 22 * l) / 451;
        int n = (h + l - 7 * m + 114) / 31;
        int p = (h + l - 7 * m + 114) % 31;
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.clear();
        calendar.set(year, n, p + 1);
        //int day = calendar.DAY_OF_MONTH;
        int day = calendar.get(Calendar.DAY_OF_MONTH);


        if (n == 3) {
            String monthIntToString = "March";

            String formattedDate = String.format("%s %d", monthIntToString, (p+1));

            return formattedDate;

        } else {
            String monthIntToString = "April";

            String formattedDate = String.format("%s %d", monthIntToString, day);

            return formattedDate;
        }
    }


    /**
     * printOccuranceOfEasterDates creates a TreeMap that holds a key:Month and value:Day;
     *When called a for loop that starts at 0 and goes to 5,700,000 runs and sorts the
     * tree. After the tree is computed it is printed out via the console.
     *
     */
    public static void printOccuranceOfEasterDates() {

        TreeMap<String, Integer> easterOccurrences = new TreeMap<String, Integer>();

        for (int i = 0; i <= 5700000; i++) {
            String easterDate = S3_Computus_Medium.calculateEasterDate(i);
//            Set set = easterOccurrences.entrySet();
//            Iterator i = set.iterator();

            if (!easterOccurrences.containsKey(easterDate)) {
                easterOccurrences.put(easterDate, 1);
            } else {
                int newEasterDateCount = easterOccurrences.get(easterDate) + 1;
                //easterOccurrences.remove(easterDate);
                easterOccurrences.put(easterDate, newEasterDateCount);
            }

        }

        System.out.println("Easter over an entire cycle of 5,700,000 years:");

        for (Map.Entry<String, Integer> valueInMap: easterOccurrences.entrySet()) {
            System.out.println(valueInMap.getKey() + " - " + valueInMap.getValue());
        }
        
    }
}
