

import junit.framework.TestCase;

/**
 * Created by joshuataylor on 9/8/15.
 */
public class S3_Computus_Hard  extends TestCase {
    /**
     *
     */
    public void testComputus() {
        S3_Computus_Medium test = new S3_Computus_Medium();
        assertEquals("April 11", test.calculateEasterDate(1700));


        assertEquals("April 12", test.calculateEasterDate(1705));
        
        assertEquals("April 8", test.calculateEasterDate(1708));


        assertEquals("March 24", test.calculateEasterDate(1799));

        assertEquals("April 13", test.calculateEasterDate(1800));


        assertEquals("April 2", test.calculateEasterDate(1820));


        assertEquals("April 11", test.calculateEasterDate(1830));


        assertEquals("April 20", test.calculateEasterDate(1851));


        assertEquals("April 1", test.calculateEasterDate(1877));


        assertEquals("April 5", test.calculateEasterDate(1885));


        assertEquals("April 15", test.calculateEasterDate(1900));


        assertEquals("March 27", test.calculateEasterDate(1910));


        assertEquals("March 24", test.calculateEasterDate(1940));


        assertEquals("April 10", test.calculateEasterDate(1966));


        assertEquals("March 30", test.calculateEasterDate(1986));


        assertEquals("April 15", test.calculateEasterDate(1990));


        assertEquals("April 4", test.calculateEasterDate(1999));


        assertEquals("April 23", test.calculateEasterDate(2000));


        assertEquals("March 27", test.calculateEasterDate(2016));


        assertEquals("March 28", test.calculateEasterDate(2100));


        assertEquals("April 3", test.calculateEasterDate(2208));


        assertEquals("April 16", test.calculateEasterDate(2299));
        

    }
}
