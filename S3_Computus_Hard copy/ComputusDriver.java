
import java.util.Scanner;


/**
 * ComputusDriver provides a user interface for the user to enter a year to compute
 * the day and month of Easter that year.  Also this class prints to the console
 * the occurance of Easter in a 5,700,000 cycle.
 */
public class ComputusDriver {

    public static void main(String[] args){



        //Gets user inputted date then outputs corresponding Easter Date
        Scanner userInput = new Scanner(System.in);
        System.out.println("Enter a Year:");
        int userDateToTest = userInput.nextInt();

        S3_Computus_Medium e = new S3_Computus_Medium();
        String formattedWithDate = e.calculateEasterDate(userDateToTest) + " " + S3_Computus_Medium.getYear();

        System.out.print(e.calculateEasterDate(userDateToTest));
        System.out.println("\n");

        //Print off order of easter dates
        S3_Computus_Medium.printOccuranceOfEasterDates();




    }
}
