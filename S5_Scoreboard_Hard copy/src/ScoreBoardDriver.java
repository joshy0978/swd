import java.util.Scanner;

/**
 * ScoreBoardDriver creates game to be scored
 * @author joshuataylor
 *
 */
public class ScoreBoardDriver {

	public static void main(String[] args) {
		
		
		
		System.out.println("Select type of game:");
		System.out.println("1. Football");
		System.out.println("2. Basketball");
		System.out.println("3. Soccer");
		System.out.println("4. Hockey");
		System.out.println("choice: ");
		Scanner gameChoice = new Scanner(System.in);
		
		/**
		 * holds the choiceForGame
		 */
		int choiceForGame = gameChoice.nextInt();
		
		Scanner homeTeamName = new Scanner(System.in);
		System.out.println("Enter Home Team: ");
		/**
		 * holds the HomeTeam String
		 */
		String homeTeam = homeTeamName.nextLine();
		
		Scanner awayTeamName = new Scanner(System.in);
		System.out.println("Enter Away Team: ");
		/**
		 * holds the AwayTeam String
		 */
		String awayTeam = awayTeamName.nextLine();
		
		
		
		
		
		// Get user input to create teams and games.
		
		/**
		 * away Team
		 */
		Team away = new Team(awayTeam);
		/**
		 * Home Team
		 */
		Team home = new Team(homeTeam);
		
		/**
		 * Game is set depending on user input
		 */
		Game game = null;
		if (choiceForGame == 1) {
			game = new Football(away, home);
		} else if (choiceForGame == 2) {
			game = new Basketball(away, home);
		} else if (choiceForGame == 3) {
			game = new Soccer(away, home);
		} else if (choiceForGame == 4) {
			game = new Hockey(away, home);
		}
		
		//if statments to set up game
		Scanner scanner = new Scanner(System.in);
		
		while(!game.isGameOver()) {
			System.out.println(game.getScoringMenu());
			System.out.println("Enter Choice: ");
			int choice = scanner.nextInt();
			if( choice <= game.scoringMethods.size() ) {
				//home
				ScoringMethod sm = game.getScoringMethods().get(choice - 1);
				game.addScore(sm, home);
			} else if( choice <= game.scoringMethods.size() * 2 )  {
				//away
				int numbersm = game.getScoringMethods().size();
				ScoringMethod sm = game.getScoringMethods().get(choice - numbersm - 1);
				game.addScore(sm, away);
			} else if( choice == game.scoringMethods.size() * 2 + 1 ) {
				if (game.getCurrentPeriod() == game.numberOfPeriods) {
					game.setEndOfGame(true);
				} else {
					game.increaseCurrentPeriod();
					game.getCurrentPeriod();
				}
			}
			System.out.println(game.getAwayTeamName() + " - " + game.getAwayTeamScore() + ", " + game.getHomeTeamName() + "- " + game.getHomeTeamScore());
			System.out.println("Current " + game.getPeriodName() + ": " + game.getCurrentPeriod());
		}
		System.out.println("Game is Over");
		System.out.println(game.getAwayTeamName() + " - " + game.getAwayTeamScore() + ", " + game.getHomeTeamName() + "- " + game.getHomeTeamScore());
		System.out.println("Current " + game.getPeriodName() + ": " + "Final");
		System.out.println(game.winningTeam());
		
	}

}
