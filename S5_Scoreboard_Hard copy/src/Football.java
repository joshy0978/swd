/**
 * Football game class
 * @author joshuataylor
 *
 */
public class Football extends Game {

	/**
	 * constructor that sets the super class homeTeam and AwayTeam instance variables
	 * @param homeTeam homeTeam
	 * @param awayTeam awayTeam
	 */
	public Football(Team homeTeam, Team awayTeam) {
		super(homeTeam, awayTeam);
		this.numberOfPeriods = 4;
		this.periodName = "Quater";
		
		// Initialize scoring methods.
		scoringMethods.add(new ScoringMethod("touchdown", 6));
		scoringMethods.add(new ScoringMethod("field goal", 3));
		scoringMethods.add(new ScoringMethod("extra-point", 1));
		scoringMethods.add(new ScoringMethod("two-point converstion", 2));
		scoringMethods.add(new ScoringMethod("safety", 2));
	}
	
}
