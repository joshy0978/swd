/**
 * ScoringMethod is a class that creates a type of scoring method for
 * a game type. It gives a name and a value.
 * @author joshuataylor
 *
 */
public class ScoringMethod {
	/**
	 * Name of the scoring method
	 */
	private String name;
	
	/**
	 * Point value of scoring method
	 */
	private int value;

	/**
	 * constructor that sets the scoring method
	 * @param name string that sets the name of the class
	 * @param value int value that sets the value of the class
	 */
	public ScoringMethod(String name, int value) {
		
		this.name = name;
		this.value = value;
		// TODO Auto-generated constructor stub
	}

	/**
	 * getter for the string name instance variable
	 * @return name instance variable 
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * getter for the string name instance variable
	 * @return int value instance variable
	 */
	public int getValue() {
		return value;
	}


}
