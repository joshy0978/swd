
public class Team {
	
	//Instance Variable
	final private String teamName;
	private int score = 0;

	//Constructor
	public Team(String teamName) {
		this.teamName = teamName;
	}

	public String getTeamName() {
		return teamName;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

}
