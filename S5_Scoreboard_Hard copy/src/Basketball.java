/**
 * Baseketball game class
 * @author joshuataylor
 *
 */
public class Basketball extends Game {

	/**
	 * constructor that sets the super class homeTeam and AwayTeam instance variables
	 * @param homeTeam homeTeam
	 * @param awayTeam awayTeam
	 */
	public Basketball(Team homeTeam, Team awayTeam) {
		super(homeTeam, awayTeam);
		this.numberOfPeriods = 4;
		this.periodName = "Quarter";
		
		// Initialize scoring methods.
		scoringMethods.add(new ScoringMethod("2 pointer", 2));
		scoringMethods.add(new ScoringMethod("3 pointer", 3));
		scoringMethods.add(new ScoringMethod("free throw", 1));
	}

}
