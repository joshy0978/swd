/**
 * Hockey game class
 * @author joshuataylor
 *
 */
public class Hockey extends Game {

	/**
	 * constructor that sets the super class homeTeam and AwayTeam instance variables
	 * @param homeTeam homeTeam
	 * @param awayTeam awayTeam
	 */
	public Hockey(Team homeTeam, Team awayTeam) {
		super(homeTeam, awayTeam);
		this.numberOfPeriods = 3;
		this.periodName = "Period";
		
		// Initialize scoring methods.
		scoringMethods.add(new ScoringMethod("goal", 1));
		scoringMethods.add(new ScoringMethod("penalty shoot", 1));
	}
}
