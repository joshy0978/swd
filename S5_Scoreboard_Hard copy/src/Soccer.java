/**
 * Soccer game class.  
 * @author joshuataylor
 *
 */
public class Soccer extends Game {

	/**
	 * constructor that sets the super class homeTeam and AwayTeam instance variables
	 * @param homeTeam homeTeam
	 * @param awayTeam awayTeam
	 */
	public Soccer(Team homeTeam, Team awayTeam) {
		super(homeTeam, awayTeam);
		this.numberOfPeriods = 2;
		this.periodName = "Half";
		
		// Initialize scoring methods.
		scoringMethods.add(new ScoringMethod("goal", 1));
		scoringMethods.add(new ScoringMethod("penalty shoot", 1));
	}

}
