import java.util.ArrayList;
/**
 * abstract class that every object of type Game inherrites
 * @author joshuataylor
 *
 */
public abstract class Game {
	/**
	 * array to hold teams
	 */
    private Team[] teams = new Team[2]; 
    
    /**
     * sets the period name
     */
    protected String periodName;
    /**
     * the number of periods
     */
    protected int numberOfPeriods;
    /**
     *  end of game 
     */
    private boolean endOfGame = false;
    public boolean isEndOfGame() {
		return endOfGame;
	}


	public void setEndOfGame(boolean endOfGame) {
		this.endOfGame = endOfGame;
	}

	public void increaseCurrentPeriod() {
		currentPeriod++;
	}

	/**
     * array list to hold scorring methods
     */
    protected ArrayList<ScoringMethod> scoringMethods;


    /**
     * keeps track of the current period
     */
	private int currentPeriod = 1;
	

	
	
	


    


	public int getCurrentPeriod() {
		return currentPeriod;
	}


	public void setCurrentPeriod(int currentPeriod) {
		this.currentPeriod = currentPeriod;
	}


	/**
	 * geter for periodName
	 * @returnperiodName instance variable
	 */
	public String getPeriodName() {
		return periodName;
	}


	/**
	 * set the periodName instance variable
	 * @param periodName
	 */
	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}


	/**
	 * constructor that sets the teams
	 * @param homeTeam type Team 
	 * @param awayTeam type Team
	 */
	public Game(Team homeTeam, Team awayTeam) {
		// TODO Auto-generated constructor stub
//		this.homeTeam = homeTeam;
//		this.awayTeam = awayTeam;
		
		teams[0] = homeTeam;
		teams[1] = awayTeam;
		scoringMethods = new ArrayList<ScoringMethod>();
	}
	
	
	/**
	 *  gets the home team score
	 * @return team score instance variable
	 */
	public int getHomeTeamScore() {
		
		return teams[0].getScore();
		//return homeTeamScore;
	}

	/**
	 * sets the home team score instance variable
	 * @param homeTeamScore value of home team score
	 */
	public void setHomeTeamScore(int homeTeamScore) {
		teams[0].setScore(homeTeamScore);
		
		//this.homeTeamScore = homeTeamScore;
	}

	/**
	 *  gets the away team score
	 * @return team score instance variable
	 */
	public int getAwayTeamScore() {
		return teams[1].getScore();
		
		//return awayTeamScore;
	}

	/**
	 * sets the away team score instance variable
	 * @param awayTeamScore value of away team score
	 */
	public void setAwayTeamScore(int awayTeamScore) {
		teams[1].setScore(awayTeamScore);
		
		//this.awayTeamScore = awayTeamScore;
	}
	
	/**
	 * 
	 * @return string value of HomeTeam Name class
	 */
	public String getHomeTeamName() {
		
		return teams[0].getTeamName();
		
		
		//return this.homeTeam.getTeamName();
	}
	
	/**
	 * get away team name instance variable
	 * @return away team name instance variable
	 */
	public String getAwayTeamName() {
		
		return teams[1].getTeamName();
		//return this.awayTeam.getTeamName();
	}
	
	/**
	 * add the score
	 * @param scoringMethod scoring method type
	 * @param team team variable
	 */
	public void addScore(ScoringMethod scoringMethod, Team team) {
		
		int teamScore = team.getScore();
		team.setScore(scoringMethod.getValue() + teamScore);
		
	}
	
	/**
	 * isGameover check
	 * @return false
	 */
	public boolean isGameOver() {
		
		return isEndOfGame();
	}
	
	/**
	 * returns the instance varible periodName
	 * @return periodName instance variable
	 */
	public String getNameOfPeriod() {
		return periodName;
	}
	/**
	 * getlenghtofPeriod
	 * @return period length
	 */
	public int getLengthOfPeriod() {
		// Create variable for this instead.
		return 0;
	}
	
	/**
	 * the menu to keep score classes
	 * @return
	 */
	public String getScoringMenu() {
		
		String menu = "Menu:\n";
		int optionNumber = 1;
		
		for (ScoringMethod scoringMethod : scoringMethods) {
			menu += optionNumber + ". "+ teams[0].getTeamName() + " " +scoringMethod.getName() +"\n";
			optionNumber++;
		}
		
		for (ScoringMethod scoringMethod : scoringMethods) {
			menu += optionNumber + ". "+ teams[1].getTeamName() + " " +scoringMethod.getName() + "\n";
			optionNumber++;
		}
		
		menu += optionNumber + ". End " + periodName;
		
		return menu;
	}
	
	/**
	 * Prints out the winning team
	 * @return
	 */
	public String winningTeam() {
		
		
			if ( teams[0].getScore() > teams[1].getScore()) {
				
				String winner = String.format("Winner: " + teams[0].getTeamName());
				
				return winner;
			} else {
				String winner = String.format("Winner: " + teams[1].getTeamName());
				return winner;
			}
		
		
	
		
	}
	
	
	/**
	 * sets the endofgame value to true
	 */
	public void setEndOfGame() {
		this.endOfGame = true;
	}


	/**
	 * getter for arraylist
	 * @return arraylist of Scoring Methods
	 */
	public ArrayList<ScoringMethod> getScoringMethods() {
		return scoringMethods;
	}

	
	
	


}
