/**
 * FrmaeBuilder is the 3rd Filter in the process
 * @author joshuataylor
 *
 */
public class FrameBuilder extends Filter implements Runnable{

	/**
	 * constructor to set input and output buffer
	 * @param input CircularBuffer
	 * @param output CircularBuffer
	 */
    public FrameBuilder (CircularBuffer input, CircularBuffer output) {

        super(input, output);
    }

  
    @Override
	public void run() {

        try {


            while (true) {


                String inputString = inputBuffer.blockingGet();
                StringBuilder outputString = new StringBuilder(inputString);

                System.out.println("FrameBuilder input From Reverse: " + inputString.toString());
                outputBuffer.blockingPut(outputString.toString());


                StringBuilder inputMinusFirstBase = new StringBuilder(inputString).delete(0,1);
                System.out.println("FrameBuilder inputMinusFirstBase: " + inputMinusFirstBase.toString());

                outputBuffer.blockingPut(inputMinusFirstBase.toString());

                StringBuilder inputMinusFirstSecondBase = new StringBuilder(inputString).delete(0,2);
                System.out.println("FrameBuilder inputMinusFirstSecondBase: " + inputMinusFirstSecondBase.toString());

                outputBuffer.blockingPut(inputMinusFirstSecondBase.toString());

//                outputString.append(inputString);
//                outputString.append(inputMinusFirstBase);
//                outputString.append(inputMinusFirstSecondBase);





            }





        } catch (InterruptedException exception) {
            Thread.currentThread().interrupt();
        }


    }



}
