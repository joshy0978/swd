/**
 * FrmaeBuilder is the 1st Filter in the process
 * @author joshuataylor
 *
 */
public class GapFilter extends Filter implements Runnable{


	/**
	 * constructor to set input and output buffer
	 * @param input CircularBuffer
	 * @param output CircularBuffer
	 */
    public GapFilter(CircularBuffer inputBuffer, CircularBuffer outputBuffer) {

        super(inputBuffer, outputBuffer);

    }

    @Override
	public void run() {

        try {

            String inputString = inputBuffer.blockingGet();
            System.out.println("Input From InputFilter: " + inputString);
            StringBuilder stringWithNoSpaces = new StringBuilder();


            for (int i = 0; i < inputString.length(); i++) {
                char c = inputString.charAt(i);
                if (c == 'N') {
                    System.out.println("stringWithNoSpaces: " + stringWithNoSpaces.toString());
                    outputBuffer.blockingPut(stringWithNoSpaces.toString());
                    stringWithNoSpaces.setLength(0);
                }
                if ((c == ' ')) {

                }
                if (c == 'A' || c == 'C' || c == 'G' || c == 'T') {
                    stringWithNoSpaces.append(c);
                }

            }


        } catch (InterruptedException exception) {
                Thread.currentThread().interrupt();
        }


    }


}
