/**
 * InputFilter passes input data to the first filter(Gap)
 * @author joshuataylor
 *
 */
public class InputFilter implements Runnable{

	/**
	 * String value to test through filters
	 */
    private final String inputString;
    /**
     * connects to CircularBuffer to make sure data being based is thread safe
     */
    private final CircularBuffer outputbuffer;

    /**
     * Inputfilter sets the input String to start the filter process
     * @param input string to test
     * @param outputbuffer to connect to input of 1st filter
     */
    public InputFilter(String input, CircularBuffer outputbuffer) {

        this.inputString = input;
        this.outputbuffer = outputbuffer;

    }

    @Override
	public void run() {



        try {
            outputbuffer.blockingPut(inputString);


        } catch (InterruptedException exception) {
            Thread.currentThread().interrupt();
        }


    }

}
