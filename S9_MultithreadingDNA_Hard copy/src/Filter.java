/**
 * Filter sets up the Input and Output buffer
 * @author joshuataylor
 *
 */
public class Filter {

	/**
	 * CircularBuffer inputBuffer 
	 */
    protected final CircularBuffer inputBuffer;
    /**
     *  CircularBuffer outputBuffer
     */
    protected final CircularBuffer outputBuffer;

    /**
     * Default Constructs sets outputBuffer and inputBuffer each to null;
     */
    public Filter () {
        this.outputBuffer = null;
        this.inputBuffer = null;
    }
    
    /**
     * CConsturct that passes an inputBuffer and outputBuffer
     * @param inputBuffer that is from an output buffer
     * @param outputBuffer connects to an input buffer from another object
     */
    public Filter (CircularBuffer inputBuffer, CircularBuffer outputBuffer) {

        this.inputBuffer = inputBuffer;
        this.outputBuffer = outputBuffer;

    }



}

