import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Creates a threadpool that hold buffers and filters
 * @author joshuataylor
 *
 */
public class DNADriver {

    public static void main(String[] args) throws InterruptedException {

        // 
    	/**
    	 * create new thread pool with threads
    	 */
        ExecutorService executorService = Executors.newCachedThreadPool();

        //
        /**
         * Buffer Bwtween Input and Gap(1)
         */
        CircularBuffer bufferBetweenInputandGap = new CircularBuffer();

        
        /**
         *  Buffer Between Gap(1) and Reverse Com(2)
         */
        CircularBuffer bufferBetweenGapAndReverse = new CircularBuffer();

        //
        /**
         * Buffer Between Reverse Com(2) and Frame(3)
         */
        CircularBuffer bufferBetweenReverseComAndFrame = new CircularBuffer();

        //
        /**
         * Buffer Between Frame(3) and Translator(4)
         */
        CircularBuffer bufferBetweenFrameAndTranslator = new CircularBuffer();

        //
        /**
         * Buffer Between Translator(4) and ORF(5)
         */
        CircularBuffer bufferBeweenTranslatorAndORF = new CircularBuffer();

        //
        /**
         * Buffer Between ORF(5) and output
         */
        CircularBuffer bufferBetweenORFAndOutput = new CircularBuffer();

        //Filter1

        executorService.execute(new InputFilter("A T  C  G N  A T N C C G  G C A T   N T A C T  A A C T G G T G C A T G A A C C T T G G A A A A A A C T G C T A G A A C T G T G C A G A C T A G T A G T T T A G C CC G G G G G G G G G A C T N", bufferBetweenInputandGap) );

        //connect output to input of filter2
        executorService.execute(new GapFilter(bufferBetweenInputandGap, bufferBetweenGapAndReverse));

        executorService.execute(new ReverseComplementor(bufferBetweenGapAndReverse, bufferBetweenReverseComAndFrame));

        executorService.execute(new FrameBuilder(bufferBetweenReverseComAndFrame, bufferBetweenFrameAndTranslator));

        executorService.execute(new Translator(bufferBetweenFrameAndTranslator, bufferBeweenTranslatorAndORF));

        executorService.execute(new ORFFinder(bufferBeweenTranslatorAndORF,bufferBetweenORFAndOutput));

        executorService.execute(new OutputFilter(bufferBetweenORFAndOutput));


        executorService.shutdown();



        


    }
}
