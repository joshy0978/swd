/**
 * CircularBuffer class is buffer between each filter
 * @author joshuataylor
 *
 */
public class CircularBuffer {

	/**
	 * buffer is an array that holds value that are
	 * passes between each filter
	 */
    private final String[] buffer = new String[10]; //Shared Buffer



    private int occupiedCells = 0; //count number of buffers used
    private int writeIndex = 0; // index of next element to write to
    private int readIndex = 0; // index of next element to read

    //place value in buffer
    /**
     * Synchronized put method for buffer array
     * @param value String value to add to buffer array
     * @throws InterruptedException
     */
    public synchronized void blockingPut(String value) throws InterruptedException {

        //wait until buffer has space available, then write value;
        //while no empty locations, place thread in blocked state
        while (occupiedCells == buffer.length) {
            System.out.println("Buffer is full. Producer waits");
            wait();
        }

        buffer[writeIndex] = value; //set new buffer value
        System.out.println();

        //update circular write index
        writeIndex = (writeIndex + 1) % buffer.length;

        ++occupiedCells; // one more buffer cell is full
        notifyAll(); // notify threads waiting to read from buffer
    }

    /**
     * blockingGet method blocking other threads from accessing the buffer while
     * one thread is already acceessing it
     * @return string value from buffer
     * @throws InterruptedException
     */
    public synchronized String blockingGet() throws InterruptedException {

        //wait until buffer has data, then read value;
        //while no data to read, place thread in waiting state
        while (occupiedCells == 0) {
            System.out.println("Buffer is empty. Consuer waits.");
            wait();
        }

        String readValue = buffer[readIndex];  //read value from buffer

        //update circular read index
        readIndex = ( readIndex + 1) % buffer.length;

        --occupiedCells; // one fewer buffer cells are occupied
        notifyAll();



        return readValue;

    }




}
