
/**
 * 
 * Enum class was given to me.  Each item in the enum is an object. 
 *
 */
    public enum Codon {
    UUU("Phe", 'F'),
    UUC("Phe", 'F'), UUA("Leu", 'L'), UUG("Leu", 'L'),
    CUU("Leu", 'L'), CUC("Leu", 'L'), CUA("Leu", 'L'), CUG("Leu", 'L'),
    AUU("Ile", 'I'), AUC("Ile", 'I'), AUA("Ile", 'I'), AUG("Met", 'M'),
    GUU("Val", 'V'), GUC("Val", 'V'), GUA("Val", 'V'), GUG("Val", 'V'),
    UCU("Ser", 'S'), UCC("Ser", 'S'), UCA("Ser", 'S'), UCG("Ser", 'S'),
    CCU("Pro", 'P'), CCC("Pro", 'P'), CCA("Pro", 'P'), CCG("Pro", 'P'),
    ACU("Thr", 'T'), ACC("Thr", 'T'), ACA("Thr", 'T'), ACG("Thr", 'T'),
    GCU("Ala", 'A'), GCC("Ala", 'A'), GCA("Ala", 'A'), GCG("Ala", 'A'),
    UAU("Tyr", 'Y'), UAC("Tyr", 'Y'), UAA("Stop", '*'), UAG("Stop", '*'),
    CAU("His", 'H'), CAC("His", 'H'), CAA("Gln", 'Q'), CAG("Gln", 'Q'),
    AAU("Asn", 'N'), AAC("Asn", 'N'), AAA("Lys", 'K'), AAG("Lys", 'K'),
    GAU("Asp", 'D'), GAC("Asp", 'D'), GAA("Glu", 'E'), GAG("Glu", 'E'),
    UGU("Cys", 'C'), UGC("Cys", 'C'), UGA("Stop", '*'), UGG("Trp", 'W'),
    CGU("Arg", 'R'), CGC("Arg", 'R'), CGA("Arg", 'R'), CGG("Arg", 'R'),
    AGU("Ser", 'S'), AGC("Ser", 'S'), AGA("Arg", 'R'), AGG("Arg", 'R'),
    GGU("Gly", 'G'), GGC("Gly", 'G'), GGA("Gly", 'G'), GGG("Gly", 'G');

    	
    private String threeLetterCode;
    private char oneLetterCode;

    /**
     * constructor for an Codon object
     * @param threeLetter 
     * @param oneLetter
     */
    private Codon(String threeLetter, char oneLetter) {
        threeLetterCode = threeLetter;
        oneLetterCode = oneLetter;
    }
    /**
     * returns the threelettercode instance variable 
     * @return string threeLetterCode
     */
    public String getThreeLetterCode() {
        return threeLetterCode;
    }

    /**
     * getter method that returns the instance variable oneLetterCode
     * @return 
     */
    public char getOneLetter() {
        return oneLetterCode;
    }

    
    /**
     * Returns a Codon object 
     * @param codon string value of enum constant
     * @return a Codon object
     */
    public static Codon getCodon(String codon) {
        codon =codon.toUpperCase();
        codon = codon.replace('T','U');
        return valueOf(codon);
    }
}

