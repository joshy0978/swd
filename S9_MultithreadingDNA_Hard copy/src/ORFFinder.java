/**
 * ORFFinder is the 5th Filter in the process
 * @author joshuataylor
 *
 */
public class ORFFinder extends Filter implements Runnable {



	/**
	 * constructor to set input and output buffer
	 * @param input CircularBuffer
	 * @param output CircularBuffer
	 */
    public ORFFinder (CircularBuffer inputBuffer, CircularBuffer outputBuffer) {
        super (inputBuffer, outputBuffer);
    }


    @Override
	public void run() {

        try {
        	StringBuilder output = new StringBuilder();
            while (true) {
                String inputFromTranslator = inputBuffer.blockingGet();

                System.out.println("ORF FILTER: input " + inputFromTranslator);

                StringBuilder stringToTestFromInput = new StringBuilder(inputFromTranslator);
                
                //stringToTestFromInput.append(inputFromTranslator);


                int currentCharToTest = 0;
                boolean inORF = false;
                while (currentCharToTest < stringToTestFromInput.length()) {
                	if (stringToTestFromInput.charAt(currentCharToTest) == '*' && !inORF) {
                		inORF = true;
                		output.setLength(0);
                	} else if (stringToTestFromInput.charAt(currentCharToTest) == '*' && inORF) {
                		if(output.length() > 21) {
							outputBuffer.blockingPut(output.toString());
						}
                		output.setLength(0);
                		inORF = false;
                	} else if (inORF) {
                		output.append(stringToTestFromInput.charAt(currentCharToTest));
                	}
                	currentCharToTest++;
                }
            }

        } catch (InterruptedException exception) {
            Thread.currentThread().interrupt();
        }



    }

}
