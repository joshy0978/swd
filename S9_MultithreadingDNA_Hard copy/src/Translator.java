/**
 * Translator is the 2nd Filter in the process
 * @author joshuataylor
 *
 */
public class Translator extends Filter implements Runnable{

	/**
	 * StringBuilder outputString is used to pass data to output buffer
	 */
	StringBuilder outputString = new StringBuilder();

	/**
	 * constructor to set input and output buffer
	 * @param input CircularBuffer
	 * @param output CircularBuffer
	 */
    public Translator (CircularBuffer input, CircularBuffer output) {
        super( input, output);
    }


    @Override
	public void run() {

        try {
            while (true) {

                String inputStringFromFrame = inputBuffer.blockingGet();


                StringBuilder stringFromInput = new StringBuilder(inputStringFromFrame);
                System.out.println("TRANSLATOR FILTER: stringFromFrameBuilder: " + inputStringFromFrame.toString());
                
                if(inputStringFromFrame.length() >  3) { 
                	outputString.setLength(0);
	                while (stringFromInput.length() >= 3) {
	
	                    String threeCharacterStringFromInput = stringFromInput.substring(0,3);
	                    stringFromInput.delete(0,3);
	
	                    Codon codon = Codon.getCodon(threeCharacterStringFromInput);
	                    outputString.append(codon.getOneLetter());
	
	                }
	                
	                System.out.println("TRANSLATOR FILTER: outputString: " + outputString.toString());
	                outputBuffer.blockingPut(outputString.toString());
                }

            }
        } catch (InterruptedException exception) {
            Thread.currentThread().interrupt();
        }
    }

}
