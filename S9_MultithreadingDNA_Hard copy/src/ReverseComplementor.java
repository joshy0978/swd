/**
 * ReverseComplementor is the 2nd Filter in the process
 * @author joshuataylor
 *
 */
public class ReverseComplementor extends Filter implements Runnable{


	/**
	 * StringBuilder outputString is used to pass data to output buffer
	 */
    StringBuilder outputString = new StringBuilder();

    /**
	 * constructor to set input and output buffer
	 * @param input CircularBuffer
	 * @param output CircularBuffer
	 */
    public ReverseComplementor (CircularBuffer input, CircularBuffer output) {
        super(input, output);
    }

    @Override
	public void run() {

            try {

                while (true) {


                    String inputFromGapFilter = inputBuffer.blockingGet();

                    StringBuilder stringToReverse = new StringBuilder(inputFromGapFilter);

                    //output orignal string to Frame Filter
                    outputBuffer.blockingPut(stringToReverse.toString());

                    //reverse stringToReverse
                    stringToReverse.reverse();


                    StringBuilder reverseComplementOutputString = new StringBuilder();

                    String stringForLoop = stringToReverse.toString();

                    for (int i = 0; i < stringForLoop.length(); i++) {
                        char c = stringForLoop.charAt(i);
                        if (c == 'A') {
                            reverseComplementOutputString.append('T');
                        }
                        if (c == 'T') {
                            reverseComplementOutputString.append('A');
                        }
                        if (c == 'C') {
                            reverseComplementOutputString.append('G');
                        }
                        if (c == 'G') {
                            reverseComplementOutputString.append('C');
                        }
                    }

                    outputBuffer.blockingPut(reverseComplementOutputString.toString());
                    System.out.println("Reverse output String: " + reverseComplementOutputString);



                }




            } catch (InterruptedException exception) {
                Thread.currentThread().interrupt();
            }



    }
}
