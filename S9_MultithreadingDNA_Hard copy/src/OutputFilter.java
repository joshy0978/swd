/**
 * Output passes input data to the first filter(Gap)
 * @author joshuataylor
 *
 */
public class OutputFilter implements Runnable{

	/**
     * connection from 4th 
     */
    private final CircularBuffer inputBuffer;

    /**
     * constructor that sets the CircularBuffer instance variable. 
     * @param inputBuffer
     */
    public OutputFilter(CircularBuffer inputBuffer) {


        this.inputBuffer = inputBuffer;

    }

    @Override
	public void run() {



        try {

            while(true) {

                String stringFromORFFilter = inputBuffer.blockingGet();
                System.out.println("************ Output: " + stringFromORFFilter);

                

            }




        } catch (InterruptedException exception) {
            Thread.currentThread().interrupt();
        }


    }



}
