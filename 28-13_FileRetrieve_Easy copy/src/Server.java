import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;

import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 * Server extends JFrame that 
 * creates the GUI for the Server
 * and sets up the internals of the Server
 * @author joshuataylor
 *
 */
public class Server extends JFrame {
	
	
	/**
	 * displayConnectionStatus is type JTextArea and 
	 * that displays information to the user
	 */
	private JTextArea displayConnectionStatus;

	/**
	 * serverSock is type ServerSocket that creates a server socket that
	 * opens on port 5252 
	 */
	private ServerSocket serverSock;

	/**
	 * output is type ObjectOutputStream that creates a output stream 
	 * that needs to be connected to a server
	 */
	private ObjectOutputStream output;
	
	/**
	 * input is type ObjectInputStream that creates a output stream
	 * that needs to be connected to a server
	 */
	private ObjectInputStream input;

	/**
	 * connection is type Socket that connects to a server socket that makes the connect
	 * of the output/input streams
	 */
	private Socket connection;

	/**
	 * messageFromClient is type String that holds a string from the client that is then processed
	 */
	private String messageFromClient;

	/**
	 * fileInfoFromClient is type File that creates a File object that is used to check the if
	 * the file requested from client exists. 
	 */
	private File fileInfoFromClient;

	/**
	 * Constructor that sets up the UI.
	 */
	public Server() {

		// sets up the jframe
		super("Server");

		displayConnectionStatus = new JTextArea("");

		displayConnectionStatus.setEditable(false);

		add(displayConnectionStatus);

		setSize(250, 300);
		setVisible(true);

	}

	/**
	 * Opens a port a client will connect to.  In addition the methods waitForConnection(), getStreams(), processConnection(), 
	 * and closeConnection() are called. 
	 */
	public void runServer() {

		try {
			serverSock = new ServerSocket(5252);

			while (true) {
				try {
					waitForConnection(); // wait for a connection
					getStreams();
					processConnection();
				} catch (EOFException eofException) {
					System.out.println("\nServer terminated connection");
				} finally {
					closeConnection();
				}
			}
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}

	}

	/**
	 * Waits for a client to connect to the server
	 * @throws IOException if connection is interrupted 
	 */
	private void waitForConnection() throws IOException {
		displayConnectionStatus.setText("Server Running on Port 5252");
		connection = serverSock.accept(); // allow server to accept connection
	}

	/**
	 * Connects the outputstream and inputstream 
	 * @throws IOException if an error in the connection occurs.
	 */
	private void getStreams() throws IOException {
		output = new ObjectOutputStream(connection.getOutputStream());
		output.flush();

		input = new ObjectInputStream(connection.getInputStream());

		displayConnectionStatus.append("\nI/O Streams Connected");

	}

	/**
	 * Closes ObjectOutputStream, ObjectInputStream, and Socket to the Server.
	 */
	private void closeConnection() {
		System.out.println("closeConnection getting called");
		displayConnectionStatus.append("\nConnection Terminated");

		try {
			output.close();
			input.close();
			connection.close();
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}

	}

	
	/**
	 * Receives request for a client for a file.  If the file exist then file is sent to client and the connection is closed.  If 
	 * the files doesn't exist a message is sent to the client letting them know that.
	 * @throws IOException stream is interrupted
	 */
	private void processConnection() throws IOException {

		messageFromClient = "";

		while (!(messageFromClient == "Close Connection")) {

			try {
				
				Object objectFromInputStream = input.readObject();
				
				messageFromClient = (String) objectFromInputStream;
				System.out.println("messageFromClient " + messageFromClient);
				System.out.println(messageFromClient);

				fileInfoFromClient = new File(messageFromClient); 
																	
				if (fileInfoFromClient.exists()) {
					System.out.println("file exists");
					// get file and send it

					System.out.println(fileInfoFromClient.length());
					byte[] fileContent = Files.readAllBytes(fileInfoFromClient.toPath());
					output.writeObject(fileContent);
					output.flush();

					messageFromClient = "Close Connection";
					
					output.writeObject("Close Connection");
					output.flush();

				} else {
					output.writeObject("SERVER>>> File Not Exist");
					output.flush();
				}

			} catch (ClassNotFoundException classNotFound) {
				System.out.println("Class not found");
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}

		}

	}

}
