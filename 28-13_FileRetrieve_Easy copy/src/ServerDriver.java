import javax.swing.JFrame;


/**
 * Instantiates a server object. 
 * @author joshuataylor
 *
 */
public class ServerDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stu
		Server server = new Server();
		server.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		server.runServer();
	}

}
