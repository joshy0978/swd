import javax.swing.JFrame;

/**
 * instantiates a client object that gets the 
 * ip address of the server and passes it to the client object
 * @author joshuataylor
 *
 */
public class ClientDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Client client;
		
		if (args.length == 0) {
			client = new Client("127.0.0.1");
		}
		else {
			client = new Client(args[0]);
		}
		client.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		client.runClient();
	}

}
