import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.file.Files;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * Client extends JFrame.  Client is a class that 
 * creates a GUI that allow a user to interact with a
 * server.  
 * @author joshuataylor
 *
 */
public class Client extends JFrame {

	// port 5252
	
	/**
	 * enterField is type JTextField that takes in user input
	 */
	private JTextField enterField;
	
	/**
	 * statusOfConnection is type JTextArea that displays the 
	 * status of the connection and messages from the server to
	 * the user.
	 */
	private JTextArea statusOfConnection;
	
	/**
	 * scrollPane is type JScrollPane that wraps the statusOfConnection
	 * JTextArea to make the statusOfConnection scrollable if text extents 
	 * the full length of the JTextArea a scroll bar will be added to the JTExtArea
	 */
	private JScrollPane scrollPane;

	
	/**
	 * output is type ObjectOutputStream that creates a output stream 
	 * that needs to be connected to a server
	 */
	private ObjectOutputStream output;
	
	/**
	 * input is type ObjectInputStream that creates a output stream
	 * that needs to be connected to a server
	 */
	private ObjectInputStream input;

	/**
	 * connection is type Socket that connects to a server socket that makes the connect
	 * of the output/input streams
	 */
	private Socket connection;
	
	/**
	 * fileServer is type String that holds the IP address of the server 
	 */
	private String fileServer;

	/**
	 * fileFromServer is type File that creates a File object that is used to create a 
	 * path to the new file form the server
	 */
	private File fileFromServer;



	/**
	 * Sets up the GUI of the Client and sets the IP address of the server
	 *  
	 * @param host the string that represents that IP address of the server
	 */
	public Client(String host) {
		

		super("Client");

		fileServer = host;

		statusOfConnection = new JTextArea("Connection Status");

		enterField = new JTextField();
		enterField.setEditable(true);
		enterField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				sendMessageToServer(e.getActionCommand());
				// displayMessage(e.getActionCommand());
				enterField.setText("");
			}

		});

		scrollPane = new JScrollPane(statusOfConnection);
		scrollPane.setPreferredSize(new Dimension(400, 100));

		add(enterField, BorderLayout.NORTH);
		add(scrollPane, BorderLayout.CENTER);

		setSize(300, 300);
		setVisible(true);

	}

	
	/**
	 * Calls the methods connectsToServer(), getStreams(), processConnection(), and closeConnection().
	 * 
	 */
	public void runClient() {
		try {
			connectToServer();
			getStreams();
			processConnection();
			// sendFilePathStringRequest("Test");
			// getFileFromServer();
		} catch (EOFException eofException) {
			displayMessage("Connection Terminated");
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {
			closeConnection();
		}
	}

	/**
	 * Connects the Socket of the Client to the Server.
	 * @throws IOException if an error in the connection occurs.
	 */
	private void connectToServer() throws IOException {
		displayMessage("Attempting connection");

		connection = new Socket(InetAddress.getByName(fileServer), 5252);

		displayMessage("Connected to: " + connection.getInetAddress().getHostName());
	}

	/**
	 * Connects the outputstream and inputstream 
	 * @throws IOException if an error in the connection occurs.
	 */
	private void getStreams() throws IOException {

		output = new ObjectOutputStream(connection.getOutputStream());
		output.flush();

		input = new ObjectInputStream(connection.getInputStream());

		displayMessage("Got I/O Streams");

	}

	/**
	 * Checks to see if the file requested from client exist. If so sends the file over OutputStreamObject then sends a string to 
	 * the client to end the connection. 
	 */
	private void processConnection() {

		// step 1 just send file
		String message = "";
		
		
		while (!(message == "Close Connection")) {
			try {
				
				Object objectFromInputStream = input.readObject();
				if (objectFromInputStream instanceof byte[]) {
					// what to get the new file
					System.out.println("Getting File");
					fileFromServer = new File("~/git/swd_jtylor/oral_exam2/27-13_FileRetrieve_Easy/src/output.txt");
					byte[] fileContent = (byte[]) objectFromInputStream;
					Files.write(fileFromServer.toPath(), fileContent);
					//output.flush();

					message = "Close Connection";
					output.writeObject(message);
					output.flush();
					System.out.println("eof");
				}
				else if (objectFromInputStream instanceof String) {
					message = (String) objectFromInputStream;
					statusOfConnection.append("\n" + message);
					//output.flush();
				}
			} catch (ClassNotFoundException e) {
				
				System.out.println("ClassNotFound");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("getting called");
				
				e.printStackTrace();
			}

		}

	}

	/**
	 * Closes ObjectOutputStream, ObjectInputStream, and Socket to the Server.
	 */
	private void closeConnection() {
		displayMessage("Closing Connections");

		try {
			enterField.setEditable(false);
			input.close();
			output.close();
			connection.close();
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}

	}

	/**
	 * Appends a String to the JTextArea
	 * @param messageToDisplay String value to be passes onto the 
	 */
	private void displayMessage(final String messageToDisplay) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				statusOfConnection.append("\n" + messageToDisplay);
			}
		});
	}

	/**
	 * Sends a String to the server to request information
	 * @param stringToSend
	 */
	private void sendMessageToServer(String stringToSend) {
		try {
			String string = stringToSend;
			output.writeObject(string);
			output.flush();
			displayMessage("Request for file sent");
		} catch (IOException ioException) {
			displayMessage("Message Failed to Send");
		}
	}

}
