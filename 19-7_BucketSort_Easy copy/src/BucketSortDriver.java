import java.util.Scanner;

/**
 * Instancates a BucketSort object.  Gets input from use.  Takes input from user and sorts the data.
 * @author joshuataylor
 *
 */
public class BucketSortDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/**
		 * creates a variable of type Scanner
		 */
		Scanner user_input = new Scanner(System.in);

		System.out.println("Enter numbers to be sorted.  Separate each value with a comma.");

		/**
		 * userInputStringValue is type String and holds user input
		 */
		String userInputStringValue = user_input.next();

		System.out.println(userInputStringValue);

		/**
		 * userInputStringValueArray is type String[] and holds values of string
		 * that represent values to be sorted
		 */
		String[] userInputStringValueArray = userInputStringValue.split(",");

		/**
		 * sortedArray is type String[] and holds the sorted values retured by
		 */
		String[] sortedArray = new String[userInputStringValueArray.length];
		sortedArray = BucketSort.bucketSort(userInputStringValueArray);

		System.out.println("Unsorted Array Values: ");
		for (String j : userInputStringValueArray) {
			System.out.println(j + " ");
		}

		System.out.println("Sorted Array Values:");
		for (String j : sortedArray) {
			System.out.println(j + " ");
		}
	}

}
