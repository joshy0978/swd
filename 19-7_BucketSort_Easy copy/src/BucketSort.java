import java.util.ArrayList;
import java.util.Arrays;

/**
 * BucketSort is a static Class that sorts an array using the Bucket sort method
 * @author joshuataylor
 *
 */
public class BucketSort {

	/**
	 * Takes in array of type String and returns the array from smallest to
	 * biggest value.
	 * 
	 * @param array
	 *            of type String
	 * @return String[] sorted from smallest value to biggest value
	 */
	public static String[] bucketSort(String[] array) {

		String[] arrayFromUser = new String[array.length];

		// populates arrayFromUser with values from array from input from method
		for (int i = 0; i < arrayFromUser.length; i++) {
			String stringValue = array[i];
			arrayFromUser[i] = stringValue;
		}

		// Temp array used for sorting
		ArrayList<String> tempArray = new ArrayList<String>();

		// Create Rows and Columns
		ArrayList<ArrayList<String>> bucket = new ArrayList<ArrayList<String>>(10);

		for (int i = 0; i < 10; i++) {

			ArrayList<String> row = new ArrayList<String>();

			bucket.add(i, row);

		}

		int placeValuesToInterateThrough = maxPlaceValueToSortThrough(arrayFromUser);

		int placeValueToCheck = 1;

		// while loop
		while (placeValueToCheck <= placeValuesToInterateThrough) {

			// Looping through each value of the Array from user
			for (int i = 0; i < arrayFromUser.length; i++) {

				String testValueString = arrayFromUser[i];
				char[] testValueCharArray = testValueString.toCharArray();
				int[] testValueIntArray = charArrayToIntArray(testValueCharArray);

				if (testValueIntArray.length >= placeValueToCheck) {
					int intValueToTest = testValueIntArray[testValueIntArray.length - placeValueToCheck];
					bucket.get(intValueToTest).add(testValueString);

				}

				else {
					// adds the value that is smaller then place value to row 0
					bucket.get(0).add(testValueString);
				}

			}

			// add values from bucket to temp array
			for (ArrayList<String> row : bucket) {

				if (!(row.isEmpty())) {
					for (int i = 0; i < row.size(); i++) {
						String value = row.get(i);
						tempArray.add(value);
					}

				}
				// clear row
				row.clear();
			}
			
			System.out.println("temArray size: " + tempArray.size());

			for (int i = 0; i < tempArray.size(); i++) {
				String valueFromTempArray = tempArray.get(i);
				arrayFromUser[i] = valueFromTempArray;
			}
			
			tempArray.clear();
			
			placeValueToCheck++;

		}
		
		

		return arrayFromUser;

	}

	/**
	 * Finds the length of the longest value of the array
	 * 
	 * @param array
	 * @return int value of the length of the longest value in the array
	 */
	static int maxPlaceValueToSortThrough(String[] array) {

        int[] intValueOfArray = new int[array.length];

        //convering string array to int array
        for (int i = 0; i < array.length; i++) {
            int valueAtValueAtI = Integer.valueOf(array[i]);
            intValueOfArray[i] = valueAtValueAtI;
        }


        Arrays.sort(intValueOfArray);

        int maxIntValue = intValueOfArray[intValueOfArray.length - 1];

        String max = String.valueOf(maxIntValue);

        char[] charFromString = max.toCharArray();

        return charFromString.length;

    }

	/**
	 * Converts type Char[] to Int[]
	 * 
	 * @param charToParse
	 *            type Char[]
	 * @return Int []
	 */
	static int[] charArrayToIntArray(char[] charToParse) {

		int[] intArray = new int[charToParse.length];

		for (int i = 0; i < charToParse.length; i++) {
			char charValue = charToParse[i];
			String stringValue = String.valueOf(charValue);
			intArray[i] = Integer.parseInt(stringValue);

		}

		return intArray;

	}

}
